<?php

//Themes options
add_theme_support('menus');

// Déclaration du side bar 
register_sidebar( array(
	'id' => 'blog-sidebar',
	'name' => 'Blog',
) );

// Déclaration menu secondaire
function register_secondary_menu() {
  register_nav_menu('secondary-menu',__( 'Menu Secondaire' ));
}
add_action( 'init', 'register_secondary_menu' );

// Déclaration menu principal
register_nav_menus( 
  array(
'main' => 'Menu Principal',
'footer' => 'Bas de page',
'mobile-tablette' => 'Mobile et tablette',
) );


//Appel des styles css
function energieuoi_register_styles(){

    //Style CSS
    $version = wp_get_theme()->get('Version');
    wp_register_style('stylesheet', get_template_directory_uri()."./style.css", array('bootstrap'), $version, 'all');
    wp_enqueue_style('stylesheet');

    //Bootstrap CSS
    wp_register_style('bootstrap', "https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css", array(), '5.0.0', 'all');
    wp_enqueue_style('bootstrap');

    //Bootstrap ICON
    wp_register_style('bootstrap-icon', "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css", array(), '1.4.1', 'all');
    wp_enqueue_style('bootstrap-icon');

    //FontAweaone CSS
    wp_register_style('fontawesome', "https://use.fontawesome.com/releases/v5.7.1/css/all.css", array(), '5.7.1', 'all');
    wp_enqueue_style('fontawesome');

  }
   
add_action('wp_enqueue_scripts', 'energieuoi_register_styles');


//Appel des scripts
function energieuoi_register_scripts(){
    //JQuery
    wp_deregister_script('jquery');
    wp_register_script('jquery', "https://code.jquery.com/jquery-3.4.1.slim.min.js", array(), '3.4.1', true);
    wp_enqueue_script('jquery');

    //Popper
    wp_register_script('popper', "https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js", array(), '2.9.1', true);
    wp_enqueue_script('popper');

    //Bootstrap
    wp_register_script('bootstrap', "https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js", array(), '5.0.0', true);
    wp_enqueue_script('bootstrap');

    //JScript
    wp_register_script('jscript', get_template_directory_uri(). "/js/script.js", array(), '1.0', true);
    wp_enqueue_script('jscript');
  }
   
add_action('wp_enqueue_scripts', 'energieuoi_register_scripts');


// Configuration du thème
require_once get_template_directory() . '/inc/config.php';

// Types de publication personnalisée et taxonomies
require_once get_template_directory() . '/inc/custom-post-types.php';


// Fonctionnalités
require_once get_template_directory() . '/inc/features.php';

//Image Sizes
add_image_size('my_custom_size_1', 300, 300, true);
add_image_size('my_custom_size_2', 1000, 500, true);
add_image_size('my_custom_size_3', 100, 100, true);



