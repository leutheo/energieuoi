<?php

class Bases{
	public function __construct(){
		$this->create_bases_post_type();
	}

	// Création d'un type de publication d'événements

	public function create_bases_post_type(){
		$label = array(
			'name' 					=> __('Bases', 'economieenergieuoi'),
			'singular_name' 		=> __('Base', 'economieenergieuoi'),
			'menu_name' 			=> _x('Bases', 'Admin menu name', 'economieenergieuoi'),
			'add_new' 				=> __('Ajouter une base', 'economieenergieuoi'),
			'add_new_item' 			=> __('Ajouter une base', 'economieenergieuoi'),
			'edit' 					=> __('Editer la base', 'economieenergieuoi'),
			'edit_item' 			=> __('Editer la bases', 'economieenergieuoi'),
			'new_item' 				=> __('Nouvelle base', 'economieenergieuoi'),
			'view' 					=> __('Voir la base', 'economieenergieuoi'),
			'View_item' 			=> __('Editer la bases', 'economieenergieuoi'),
			'search_items' 			=> __('Rechercher une base', 'economieenergieuoi'),
			'not_found' 			=> __('Aucune base trouvée', 'economieenergieuoi'),
			'not_found_in_trash' 	=> __('Aucune base trouvée dans la corbeille', 'economieenergieuoi'),
			'parent' 				=> __('base parent', 'economieenergieuoi'),

		);
		$args = array(
			'labels' 				=> $label,
			'public' 				=> true,
			'has_archive' 			=> true,
			'publicy_queryable' 	=> true,
			'exclude_from_search' 	=> false,
			'show_in_menu'		 	=> true,
			'hierarchical'		 	=> false,
			'rewrite'		 		=> array('slug' => 'bases'),
			'capability_type'		 	=> 'page',
		);
		register_post_type('energieuoi_bases', $args);
	}
}
new Bases();