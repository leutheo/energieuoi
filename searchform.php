<!-- Affichage du moteur de recherche -->
<form action="<?php echo home_url( '/' ); ?>" method="get">
    <input type="text" name="search" placeholder="Recherche" id="search" value="<?php the_search_query(); ?>" />
    <i class="fas fa-search"></i>
</form>
