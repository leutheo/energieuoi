<?php

/**
 * Fired during plugin deactivation
 *
 * @link       energieuoi
 * @since      1.0.0
 *
 * @package    Energieuoi
 * @subpackage Energieuoi/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Energieuoi
 * @subpackage Energieuoi/includes
 * @author     UOI <garneaubienvenu@gmail.com>
 */
class Energieuoi_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
