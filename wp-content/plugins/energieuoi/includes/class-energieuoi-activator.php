<?php

/**
 * Fired during plugin activation
 *
 * @link       energieuoi
 * @since      1.0.0
 *
 * @package    Energieuoi
 * @subpackage Energieuoi/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Energieuoi
 * @subpackage Energieuoi/includes
 * @author     UOI <garneaubienvenu@gmail.com>
 */
class Energieuoi_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
