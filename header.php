<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  
<!-- Affichage du logo -->
<header>
    <div class="menu-header container">
        <div class="logo-header">
            <a href="<?php echo home_url( '/' ); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="Logo">
            </a> 
        </div>
         
        <!-- Affichage du menus principal -->
        <?php wp_nav_menu( 
            array( 
                'theme_location' => 'main',
                'container' => 'ul', // afin d'éviter d'avoir une div autour 
                'menu_class' => 'header_menu', // ma classe personnalisée  
                ) 
            ); 
        ?>

        <!-- Affichage du moteur de recherhce -->

        <?php require_once get_template_directory().'/searchform.php'; ?>
    </div>
</header>
  
<?php wp_body_open(); ?>