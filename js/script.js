$(function(){
    $("div.card-body").hover(function(){
        $(this).addClass('corpstexte');
    }, function(){
        $(this).removeClass('corpstexte');
    });

    $(".test").hover(function(){
        $(this).addClass('test1');
    }, function(){
        $(this).removeClass('test1');
    })

    $(".card").hover(function(){
        $(this).addClass('polaroid');
    }, function(){
        $(this).removeClass('polaroid');
    })

    $('div.img').hover(function(){
        $(this).fadeOut("slow", 0.15);
    }, function(){
        $(this).fadeIn("slow", 0.15);
    })
});

