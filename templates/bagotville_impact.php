<?php
/**
 * Template Name: Bagotville_Impact_Projet
 */

?>
<?php get_header(); 

// Caroussel

$caroussel_1 = get_field('caroussel_1');
$picture_caroussel_1 = $caroussel_1['sizes']['my_custom_size_2'];
$caroussel_1_titre = get_field('caroussel_1_titre');
$caroussel_1_description = get_field('caroussel_1_description');

$caroussel_2 = get_field('caroussel_2');
$picture_caroussel_2 = $caroussel_2['sizes']['my_custom_size_2'];
$caroussel_2_titre = get_field('caroussel_2_titre');
$caroussel_2_description = get_field('caroussel_2_description');

$caroussel_3 = get_field('caroussel_3');
$picture_caroussel_3 = $caroussel_3['sizes']['my_custom_size_2'];
$caroussel_3_titre = get_field('caroussel_3_titre');
$caroussel_3_description = get_field('caroussel_3_description');


//Affichage carte 1/
$titre_1 = get_field('titre_1');

//Affichage de l'introduction
$introduction = get_field('introduction');

//Affichage graphe/
$image_graphe = get_field('image_graphe');
$picture_image_graphe = $image_graphe['sizes']['my_custom_size_1'];

// Affichage carte 1/
$sous_titre_1 = get_field('sous_titre_1');

//Affichage carte 1/
$nombre_carte_1 = get_field('nombre_carte_1');
$image_carte_1 = get_field('image_carte_1');
$picture_image_carte_1 = $image_carte_1['sizes']['my_custom_size_3'];
$description_carte_1 = get_field('description_carte_1');

//Affichage carte 2/
$nombre_carte_2 = get_field('nombre_carte_2');
$image_carte_2 = get_field('image_carte_2');
$picture_image_carte_2 = $image_carte_2['sizes']['my_custom_size_3'];
$description_carte_2 = get_field('description_carte_2');

//Affichage carte 3/
$nombre_carte_3 = get_field('nombre_carte_3');
$image_carte_3 = get_field('image_carte_3');
$picture_image_carte_3 = $image_carte_3['sizes']['my_custom_size_3'];
$description_carte_3 = get_field('description_carte_3');

//Affichage carte 4/
$nombre_carte_4 = get_field('nombre_carte_4');
$image_carte_4 = get_field('image_carte_4');
$picture_image_carte_4 = $image_carte_4['sizes']['my_custom_size_3'];
$description_carte_4 = get_field('description_carte_4');

// Affichage carte initiative/
$sous_titre_2 = get_field('sous_titre_2');
$titre_2 = get_field('titre_2');
$description_2 = get_field('description_2');
$image_carte = get_field('image_carte');
$picture_2 = $image_carte['sizes']['my_custom_size_1'];

// Affichage/
$description_3 = get_field('description_3');

// Affichage
$sous_titre_5 = get_field('sous_titre_5');
$description_5 = get_field('description_5');

$action_1 = get_field('action_1');
$action_2 = get_field('action_2');
$action_3 = get_field('action_3');
$action_4 = get_field('action_4');
$action_5 = get_field('action_5');
$action_6 = get_field('action_6');
$action_7 = get_field('action_7');
$action_8 = get_field('action_8');
$action_9 = get_field('action_9')

?>

<div class="page-wrap">
    <!-- Affichage de la bannière image -->

    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="<?php echo $picture_caroussel_1 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_1_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_1_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_2 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_2_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_2_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_3 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_3_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_3_description ?></p>
            </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <main class="container">
        <?php if( have_posts() ) {
            while( have_posts() ) {
                the_post(); ?>
        
                <?php the_content(); ?>
            
            <?php  }
            
            } ?>

        
        <div class="row">
            <div class="row-cols-1 text-center">
                <!-- Affichage titre 1 -->
                    <h2 class="text-uppercase fs-1 fw-bold text-success m-5 shadow p-3 mb-5 bg-body rounded"><?php  if ($titre_1) {?> 
                    <?php echo $titre_1; ?>  
                    <?php }?></h2> 
            </div>
        </div>
        <div class="row">
            <div class="row-cols-1">
                <!-- Affichage introduction -->
                    <p class="card-text"><?php  if ($introduction) {?> 
                        <?php echo ($introduction); ?>  
                        <?php }?></p>
            </div>
        </div>
        <div class="row">
            <!-- Affichage image carte 1 -->
            <div class="text-center">   
                <img src="<?php echo $picture_image_graphe ?>" class="card-img-top" alt="...">
            </div>
        </div>
        <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
            <div class="row-cols-1">
                <!-- Affichage sous-titre 1 -->
                    <h2 class="text-success m-5"><?php  if ($sous_titre_1) {?> 
                    <?php echo $sous_titre_1; ?>  
                    <?php }?></h2> 
            </div>
        </div>
        <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card h-100" >
                    <div class="card-body text-center">
                        <!-- Affichage description carte 1 -->
                        <h5 class="card-title"><?php  if ($description_carte_1) {?> 
                                <?php echo ($description_carte_1); ?>  
                                <?php }?></h5>
                        <!-- Affichage image carte 1 -->
                            <div class="card-img-top card-with">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/electric-car.svg" class="card-img-with-15" alt="electric-car">
                            </div>
                        <!-- Affichage en-tête carte 1 -->
                        <h4 class="ecard-text"><?php  if ($nombre_carte_1) {?> 
                                <?php echo $nombre_carte_1; ?>  
                                <?php }?></h4>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card h-100">
                    <div class="card-body text-center">
                        <!-- Affichage description carte 2 -->
                        <h5 class="card-title"><?php  if ($description_carte_2) {?> 
                                <?php echo ($description_carte_2); ?>  
                                <?php }?></h5>
                        <!-- Affichage image carte 2 --> 
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ampoulealume.svg" class="card-img-with-15" alt="ampoule">
                            </div>
                        <!-- Affichage en-tête carte 2 -->
                        <h4 class="card-text"><?php  if ($nombre_carte_2) {?> 
                                <?php echo $nombre_carte_2; ?>  
                                <?php }?></h4> 
                    </div> 
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card h-100">
                    <div class="card-body text-center">
                        <!-- Affichage description carte 3 -->
                        <h5 class="card-title"><?php  if ($description_carte_3) {?> 
                                <?php echo ($description_carte_3); ?>  
                                <?php }?></h5>
                        <!-- Affichage image carte 3 --> 
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/car.svg" class="card-img-with-15" alt="car">
                            </div>
                        <!-- Affichage en-tête carte 3 -->
                        <h4 class="card-text"><?php  if ($nombre_carte_3) {?> 
                                <?php echo $nombre_carte_3; ?>  
                                <?php }?></h4>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card h-100">
                    <div class="card-body text-center">
                        <!-- Affichage description carte 4 -->
                        <h5 class="card-title"><?php  if ($description_carte_4) {?> 
                                <?php echo ($description_carte_4); ?>  
                                <?php }?></h5>
                        <!-- Affichage image carte 4 --> 
                            <div class="card-img-top ">   
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arbre.svg" class="card-img-with-15" alt="arbre">

                            </div>
                        <!-- Affichage en-tête carte 4 -->
                            <h4 class="card-text"><?php  if ($nombre_carte_4) {?> 
                                <?php echo $nombre_carte_4; ?>  
                                <?php }?></h4> 
                    </div>
                </div>
            </div>      
        </div>
        <div class="row row-cols-1 mt-3">
            <div class="col-12">
                <div class="card h-100" >
                    <div class="card-body">
                        <!-- Affichage sous-titre 2 -->
                        <h2 class="text-success m-5"><?php  if ($sous_titre_2) {?> 
                                <?php echo ($sous_titre_2); ?>  
                                <?php }?></h2>

                            <!-- Affichage sous-titre 2 -->
                            <h4 class="card-title "><?php  if ($titre_2) {?> 
                                <?php echo $titre_2; ?>  
                                <?php }?></h4>

                        <!-- Affichage description 2 -->
                        <p class="card-text "> <?php if ($description_2) { ?>
                                <?php echo $description_2; ?>
                            <?php } ?>
                        </p>
                        <!-- Affichage image carte --> 
                        <div class="card-img-top text-center"> 
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/energy.svg" class="card-img-with-10" alt="energy">
                        </div>
                        <div class="card-text">
                        <!-- Affichage description 3 -->
                            <p> <?php if ($description_3) { ?>
                                <?php echo $description_3; ?>
                            <?php } ?>
                            </p>
                        </div>
                    </div>
                    
                </div> 
            </div>
        </div>
        <div class="row row-cols-1 mt-3">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Affichage sous-titre 5 -->
                        <h2 class="text-success m-5 "><?php  if ($sous_titre_5) {?> 
                            <?php echo ($sous_titre_5); ?>  
                            <?php }?>
                        </h2>
                        <!-- Affichage description 5 -->
                        <p class="card-text "> <?php if ($description_5) { ?>
                                <?php echo $description_5; ?>
                            <?php } ?>
                        </p>
                        <ul class="list-group">
                            <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_1) { ?>
                                <?php echo $action_1; ?>
                            <?php } ?></li>
                            <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_2) { ?>
                                <?php echo $action_2; ?>
                            <?php } ?></li>
                            <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_3) { ?>
                                <?php echo $action_3; ?>
                            <?php } ?></li>
                            <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_4) { ?>
                                <?php echo $action_4; ?>
                            <?php } ?></li>
                            <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_5) { ?>
                                <?php echo $action_5; ?>
                            <?php } ?></li>
                            <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_6) { ?>
                                <?php echo $action_6; ?>
                            <?php } ?></li>
                            <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_7) { ?>
                                <?php echo $action_7; ?>
                            <?php } ?></li>
                            <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_8) { ?>
                                <?php echo $action_8; ?>
                            <?php } ?></li>
                            <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_9) { ?>
                                <?php echo $action_9; ?>
                            <?php } ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<?php get_footer(); ?>