<?php
/**
 * Template Name: Valcartier_Projet
 */

?>
<?php get_header(); 

// Caroussel

$caroussel_1 = get_field('caroussel_1');
$picture_caroussel_1 = $caroussel_1['sizes']['my_custom_size_2'];
$caroussel_1_titre = get_field('caroussel_1_titre');
$caroussel_1_description = get_field('caroussel_1_description');

$caroussel_2 = get_field('caroussel_2');
$picture_caroussel_2 = $caroussel_2['sizes']['my_custom_size_2'];
$caroussel_2_titre = get_field('caroussel_2_titre');
$caroussel_2_description = get_field('caroussel_2_description');

$caroussel_3 = get_field('caroussel_3');
$picture_caroussel_3 = $caroussel_3['sizes']['my_custom_size_2'];
$caroussel_3_titre = get_field('caroussel_3_titre');
$caroussel_3_description = get_field('caroussel_3_description');


//Affichage titre 1/
$titre_1 = get_field('titre_1');

$introduction = get_field('introduction');

// Affichage carte 1
$sous_titre_1 = get_field('sous_titre_1');


$action_1 = get_field('action_1');
$action_2 = get_field('action_2');
$action_3 = get_field('action_3');
$action_4 = get_field('action_4');

$description = get_field('description');

$image_carte_1 = get_field('image_carte_1');
$picture_1 = $image_carte_1['sizes']['my_custom_size_1'];


// Affichage carte 2

$sous_titre_2 = get_field('sous_titre_2');

$action_5 = get_field('action_5');
$action_6= get_field('action_6');
$action_7= get_field('action_7');
$action_8 = get_field('action_8');

$image_carte_2 = get_field('image_carte_2');
$picture_2 = $image_carte_2['sizes']['my_custom_size_1'];

// Affichage carte 3

$sous_titre_3 = get_field('sous_titre_3');

$action_9 = get_field('action_9');
$action_10= get_field('action_10');

$image_carte_3 = get_field('image_carte_3');
$picture_3 = $image_carte_3['sizes']['my_custom_size_1'];


// Affichage carte 4

$sous_titre_4 = get_field('sous_titre_4');

$action_11 = get_field('action_11');
$action_12= get_field('action_12');
$action_13= get_field('action_13');
$action_14= get_field('action_14');

$image_carte_4 = get_field('image_carte_4');
$picture_4 = $image_carte_4['sizes']['my_custom_size_1'];


// Affichage carte 5

$sous_titre_5 = get_field('sous_titre_5');

$action_15= get_field('action_15');

$image_carte_5 = get_field('image_carte_5');
$picture_5 = $image_carte_5['sizes']['my_custom_size_1'];


?>

<div class="page-wrap">
    
    <!-- Affichage de la bannière image -->

    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="<?php echo $picture_caroussel_1 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_1_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_1_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_2 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_2_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_2_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_3 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_3_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_3_description ?></p>
            </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    <main class="container">
        <?php if( have_posts() ) {
            while( have_posts() ) {
                the_post(); ?>
        
                <?php the_content(); ?>
            
            <?php  }
        } ?>

        <div class="container">
            <div class="row">
                <div class="row-cols-1 text-center">
                    <!-- Affichage titre 1 -->
                        <h2 class="text-uppercase fs-1 fw-bold text-success m-5 shadow p-3 mb-5 bg-body rounded"><?php  if ($titre_1) {?> 
                        <?php echo $titre_1; ?>  
                        <?php }?></h2> 
                </div>
            </div>
            <div class="row">
                <div class="rowcols-1">
                    <!-- Affichage introduction -->
                    <p class="card-text "> <?php if ($introduction) { ?>
                        <?php echo $introduction; ?>
                        <?php } ?>
                    </p>
                </div>
            </div>
            <div class="row row-cols-1 mt-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row row-cols-1 mt-3">
                                <!-- Affichage sous-titre -->
                                <h5 class="card-text text-uppercase text-success "> <?php if ($sous_titre_1) { ?>
                                        <?php echo $sous_titre_1; ?>
                                    <?php } ?>
                                </h5>
                                <div class="col-md-12 col-lg-6">
                                    <ul class="list-group">
                                        <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_1) { ?>
                                        <?php echo $action_1; ?>
                                    <?php } ?></li>
                                        <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_2) { ?>
                                        <?php echo $action_2; ?>
                                    <?php } ?></li>
                                        <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_3) { ?>
                                        <?php echo $action_3; ?>
                                    <?php } ?></li>
                                        <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_4) { ?>
                                        <?php echo $action_4; ?>
                                    <?php } ?></li>
                                    </ul>

                                    <!-- Affichage description  -->
                                    <p class="card-text "> <?php if ($description) { ?>
                                        <?php echo $description; ?>
                                    <?php } ?>
                                    </p>
                                </div>
                               
                                <!-- Affichage image carte 1 -->
                                <div class="col-md-12 col-lg-6">
                                    <div class="card-img-top">
                                        <img src="<?php echo $picture_1 ?>"  alt="..." class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-cols-1 mt-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row row-cols-1 mt-3">
                                <!-- Affichage sous-titre -->
                                <h5 class="card-text text-uppercase text-success "> <?php if ($sous_titre_2) { ?>
                                        <?php echo $sous_titre_2; ?>
                                    <?php } ?>
                                </h5>
                                <!-- Affichage image carte 2 -->
                                <div class="col-md-12 col-lg-6">
                                    <div class="card-img-top">
                                        <img src="<?php echo $picture_2 ?>"  alt="..." class="img-fluid">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-6">
                                    <ul class="list-group">
                                        <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_5) { ?>
                                        <?php echo $action_5; ?>
                                    <?php } ?></li>
                                        <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_6) { ?>
                                        <?php echo $action_6; ?>
                                    <?php } ?></li>
                                        <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_7) { ?>
                                        <?php echo $action_7; ?>
                                    <?php } ?></li>
                                        <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_8) { ?>
                                        <?php echo $action_8; ?>
                                    <?php } ?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-cols-1 mt-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row row-cols-1 mt-3">
                                <!-- Affichage sous-titre -->
                                <h5 class="card-text text-uppercase text-success "> <?php if ($sous_titre_3) { ?>
                                        <?php echo $sous_titre_3; ?>
                                    <?php } ?>
                                </h5>
                                <div class="col-md-12 col-lg-6">
                                    <ul class="list-group">
                                        <li class="list-group-item"><?php if ($action_9) { ?>
                                        <?php echo $action_9; ?>
                                    <?php } ?></li>
                                        <li class="list-group-item"><?php if ($action_10) { ?>
                                        <?php echo $action_10; ?>
                                    <?php } ?></li>
                                    </ul>
                                </div>
                                <!-- Affichage image carte 3 -->
                                <div class="col-md-12 col-lg-6">
                                    <div class="card-img-top">   
                                        <img src="<?php echo $picture_3 ?>"  alt="..." class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-cols-1 mt-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row row-cols-1 mt-3">
                                <!-- Affichage sous-titre -->
                                <h5 class="card-text text-uppercase text-success "> <?php if ($sous_titre_4) { ?>
                                        <?php echo $sous_titre_4; ?>
                                    <?php } ?>
                                </h5>
                                <!-- Affichage image carte 4 -->
                                <div class="col-md-12 col-lg-6">
                                    <div class="card-img-top">   
                                        <img src="<?php echo $picture_4 ?>"  alt="..." class="img-fluid">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-6">
                                    <ul class="list-group">
                                        <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_11) { ?>
                                        <?php echo $action_11; ?>
                                    <?php } ?></li>
                                        <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_12) { ?>
                                        <?php echo $action_12; ?>
                                    <?php } ?></li>
                                        <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_13) { ?>
                                        <?php echo $action_13; ?>
                                    <?php } ?></li>
                                        <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_14) { ?>
                                        <?php echo $action_14; ?>
                                    <?php } ?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-cols-1 mt-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row row-cols-1 mt-3">
                                <!-- Affichage sous-titre -->
                                <h5 class="card-text text-uppercase text-success "> <?php if ($sous_titre_5) { ?>
                                        <?php echo $sous_titre_5; ?>
                                    <?php } ?>
                                </h5>
                                <div class="col-md-12 col-lg-6">
                                    <ul class="list-group">
                                        <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_15) { ?>
                                        <?php echo $action_15; ?>
                                    <?php } ?></li>
                                    </ul>
                                </div>
                                <!-- Affichage image carte 5 -->
                                <div class="col-md-12 col-lg-6">
                                    <div class="card-img-top">   
                                        <img src="<?php echo $picture_5 ?>"  alt="..." class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<?php get_footer(); ?>