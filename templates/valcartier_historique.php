<?php
/**
 * Template Name: Valcartier_Historique
 */
?>
<?php get_header(); 

// Caroussel

$caroussel_1 = get_field('caroussel_1');
$picture_caroussel_1 = $caroussel_1['sizes']['my_custom_size_2'];
$caroussel_1_titre = get_field('caroussel_1_titre');
$caroussel_1_description = get_field('caroussel_1_description');

$caroussel_2 = get_field('caroussel_2');
$picture_caroussel_2 = $caroussel_2['sizes']['my_custom_size_2'];
$caroussel_2_titre = get_field('caroussel_2_titre');
$caroussel_2_description = get_field('caroussel_2_description');

$caroussel_3 = get_field('caroussel_3');
$picture_caroussel_3 = $caroussel_3['sizes']['my_custom_size_2'];
$caroussel_3_titre = get_field('caroussel_3_titre');
$caroussel_3_description = get_field('caroussel_3_description');

//Affichage titre 1/
$titre_1 = get_field('titre_1');

//Affichage sous-titre 1/
$sous_titre_1 = get_field('sous_titre_1');

//Affichage sous-titre 2/
$sous_titre_2 = get_field('sous_titre_2');

//Affichage carte 1/
$date_1 = get_field('date_1');
$description_1 = get_field('description_1');
$date_2 = get_field('date_2');
$description_2 = get_field('description_2');
$date_3 = get_field('date_3');
$description_3 = get_field('description_3');
$date_4 = get_field('date_4');
$description_4 = get_field('description_4');

//Affichage sous-titre 3/
$sous_titre_3 = get_field('sous_titre_3');

//Affichage carte 2/
$description_5 = get_field('description_5');
$description_6 = get_field('description_6');
$description_7 = get_field('description_7');
$description_8 = get_field('description_8');

//Affichage sous-titre 4/
$sous_titre_4 = get_field('sous_titre_4');

//Affichage carte 3/
$introduction = get_field('introduction');
$date_5 = get_field('date_5');
$description_9 = get_field('description_9');
$date_6 = get_field('date_6');
$description_10 = get_field('description_10');
$date_7 = get_field('date_7');
$description_11 = get_field('description_11');

//Affichage sous-titre 5/
$sous_titre_5 = get_field('sous_titre_5');

$introduction_1 = get_field('introduction_1');

//Affichage sous-titre 6/
$sous_titre_6 = get_field('sous_titre_6');

//Affichage carte 4/
$description_12 = get_field('description_12');
$description_13 = get_field('description_13');
$description_14 = get_field('description_14');
$description_15 = get_field('description_15');
$description_16 = get_field('description_16');

//Affichage sous-titre 7/
$sous_titre_7 = get_field('sous_titre_7');

//Affichage carte 5/
$date_8 = get_field('date_8');
$description_17 = get_field('description_17');
$date_9 = get_field('date_9');
$description_18 = get_field('description_18');
$date_10 = get_field('date_10');
$description_19 = get_field('description_19');
$date_11 = get_field('date_11');
$description_20 = get_field('description_20');

//Affichage sous-titre 8/
$sous_titre_8 = get_field('sous_titre_8');

//Affichage carte 6/
$description_21 = get_field('description_21');
$description_22 = get_field('description_22');
$description_23 = get_field('description_23');
$description_24 = get_field('description_24');
$description_25 = get_field('description_25');
$description_26 = get_field('description_26');
$description_27 = get_field('description_27');

//Affichage sous-titre 9/
$sous_titre_9 = get_field('sous_titre_9');

//Affichage carte 7/
$description_28 = get_field('description_28');
$description_29 = get_field('description_29');
$description_30 = get_field('description_30');
?>

<div class="page-vrap">
    <!-- Affichage de la bannière image -->

    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="<?php echo $picture_caroussel_1 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_1_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_1_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_2 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_2_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_2_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_3 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_3_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_3_description ?></p>
            </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    <main class="container">
        <?php if( have_posts() ) {
            while( have_posts() ) {
                the_post(); ?>
        
                <?php the_content(); ?>
            
            <?php  }
            
            } ?>
        <div class="row">
            <div class="row-cols-1 text-center">
                <!-- Affichage titre 1 -->
                    <h2 class="text-uppercase fs-1 fw-bold text-success m-5 shadow p-3 mb-5 bg-body rounded"><?php  if ($titre_1) {?> 
                    <?php echo $titre_1; ?>  
                    <?php }?></h2> 
            </div>
        </div>
        <div class="row">
            <div class="row-cols-1 text-center">
                <!-- Affichage titre 1 -->
                    <h2 class=" fs-1 text-success m-5"><?php  if ($sous_titre_1) {?> 
                    <?php echo $sous_titre_1; ?>  
                    <?php }?></h2> 
            </div>
        </div>
        <div class="row m-5">
            <div class="row-cols-1 text-center">
                <!-- Affichage sous-titre 2 -->
                    <h5 class=" fs-1 text-success m-2"><?php  if ($sous_titre_2) {?> 
                    <?php echo $sous_titre_2; ?>  
                    <?php }?></h5> 
            </div>
            <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="card h-100 text-center" >
                        <!-- Affichage image carte 1 -->
                        <div class="card-img-top ">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/contrat.svg" class="top mx-auto d-block card-img-with-35" alt="contrat">                             
                        </div>
                        <div class="card-body">
                            <!-- Affichage description carte 1 -->
                                <p class="card-text "><?php  if ($date_1) {?> 
                                    <?php echo ($date_1); ?>  
                                    <?php }?></p>

                                <p class="card-text"><?php  if ($description_1) {?> 
                                    <?php echo ($description_1); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="card h-100 text-center" >
                        <!-- Affichage image carte 2 -->
                        <div class="card-img-top ">   
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/execution.svg" class="top mx-auto d-block card-img-with-35" alt="execution">  
                        </div>
                        <div class="card-body">
                            <!-- Affichage description carte 2 -->
                                <p class="card-text "><?php  if ($date_2) {?> 
                                    <?php echo ($date_2); ?>  
                                    <?php }?></p>
                                <p class="card-text"><?php  if ($description_2) {?> 
                                    <?php echo ($description_2); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="card h-100 text-center" >
                        <!-- Affichage image carte 3 -->
                        <div class="card-img-top ">   
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/performance.svg" class="top mx-auto d-block card-img-with-35" alt="performance">  
                        </div>
                        <div class="card-body">
                            <!-- Affichage description carte 3 -->
                                <p class="card-text "><?php  if ($date_3) {?> 
                                    <?php echo ($date_3); ?>  
                                    <?php }?></p>
                                <p class="card-text"><?php  if ($description_3) {?> 
                                    <?php echo nl2br($description_3); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="card h-100 text-center" >
                        <!-- Affichage image carte 4 -->
                        <div class="card-img-top ">   
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/finprojet.svg" class="top mx-auto d-block card-img-with-35" alt="finprojet">
                        </div>
                        <div class="card-body">
                            <!-- Affichage description carte 4 -->
                                <p class="card-text "><?php  if ($date_4) {?> 
                                    <?php echo ($date_4); ?>  
                                    <?php }?></p>
                                <p class="card-text"><?php  if ($description_4) {?> 
                                    <?php echo nl2br($description_4); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row m-5">
            <div class="row-cols-1 text-center">
                <!-- Affichage sous-titre 3 -->
                    <h5 class=" fs-1 text-success m-2"><?php  if ($sous_titre_3) {?> 
                    <?php echo $sous_titre_3; ?>  
                    <?php }?></h5> 
            </div>
            <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="card h-100 text-center" >
                        <!-- Affichage image carte 2 -->
                        <div class="card-img-top ">   
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fluorescent.svg" class="top mx-auto d-block card-img-with-35" alt="fluorescent">
                        </div>
                        <div class="card-body">
                            <!-- Affichage description carte 5 -->
                                <p class="card-text"><?php  if ($description_5) {?> 
                                    <?php echo ($description_5); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="card h-100 text-center" >
                        <!-- Affichage image carte 3 -->
                        <div class="card-img-top ">   
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gestionenergetique.svg" class="top mx-auto d-block card-img-with-35" alt="gestionenergetique">
                        </div>
                        <div class="card-body">
                            <!-- Affichage description carte 6 -->
                                <p class="card-text"><?php  if ($description_6) {?> 
                                    <?php echo ($description_6); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="card h-100 text-center" >
                        <!-- Affichage image carte 3 -->
                        <div class="card-img-top ">   
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/sodium.svg" class="top mx-auto d-block card-img-with-35" alt="sodium">
                        </div>
                        <div class="card-body">
                            <!-- Affichage description carte 7 -->
                                <p class="card-text"><?php  if ($description_7) {?> 
                                    <?php echo nl2br($description_7); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="card h-100 text-center" >
                        <!-- Affichage image carte 5 -->
                        <div class="card-img-top ">   
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thermique.svg" class="top mx-auto d-block card-img-with-35" alt="thermique">
                        </div>
                        <div class="card-body">
                            <!-- Affichage description carte 8 -->
                                <p class="card-text"><?php  if ($description_8) {?> 
                                    <?php echo nl2br($description_8); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-5">
            <div class="row-cols-1 text-center">
                <!-- Affichage sous-titre 4 -->
                    <h5 class=" fs-1 text-success m-2"><?php  if ($sous_titre_4) {?> 
                    <?php echo $sous_titre_4; ?>  
                    <?php }?></h5> 
            </div>
            <div class="col-12 col-md-6 col-lg-12">
                    <div class="text-center" >
                        <div class="card-body">
                            <!-- Affichage description carte 8 -->
                            <p class="card-text"><?php  if ($introduction) {?> 
                                    <?php echo nl2br($introduction); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
            <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100 text-center" >
                        <!-- Affichage image carte 3 -->
                        <div class="card-img-top ">   
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/objectif1.svg" class="top mx-auto d-block card-img-with-35" alt="objectif1">
                        </div>
                        <div class="card-body">
                            <!-- Affichage description carte 9 -->
                                <p class="card-text"><?php  if ($date_5) {?> 
                                    <?php echo ($date_5); ?>  
                                    <?php }?></p>
                                <p class="card-text"><?php  if ($description_9) {?> 
                                    <?php echo ($description_9); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100 text-center" >
                        <!-- Affichage image carte 3 -->
                        <div class="card-img-top ">   
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/objectif2.svg" class="top mx-auto d-block card-img-with-35" alt="objectif2">
                        </div>
                        <div class="card-body">
                            <!-- Affichage description carte 10 -->
                                <p class="card-text"><?php  if ($date_6) {?> 
                                    <?php echo ($date_6); ?>  
                                    <?php }?></p>
                                <p class="card-text"><?php  if ($description_10) {?> 
                                    <?php echo ($description_10); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100 text-center" >
                        <!-- Affichage image carte 3 -->
                        <div class="card-img-top ">   
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/objectif3.svg" class="top mx-auto d-block card-img-with-35" alt="objectif3">
                        </div>
                        <div class="card-body">
                            <!-- Affichage description carte 11 -->
                                <p class="card-text"><?php  if ($date_7) {?> 
                                    <?php echo ($date_7); ?>  
                                    <?php }?></p>
                                <p class="card-text"><?php  if ($description_11) {?> 
                                    <?php echo nl2br($description_11); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-5">
            <div>
                <div class="row-cols-1 text-center">
                    <!-- Affichage sous-titre 5 -->
                        <h5 class=" fs-1 text-success m-2"><?php  if ($sous_titre_5) {?> 
                        <?php echo $sous_titre_5; ?>  
                        <?php }?></h5> 
                </div>
                <div class="col-12 col-md-6 col-lg-12">
                    <div class="text-center" >
                        <div class="card-body">
                            <!-- Affichage description carte 8 -->
                            <span class="card-text"><?php  if ($introduction_1) {?> 
                                    <?php echo nl2br($introduction_1); ?>  
                                    <?php }?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="row-cols-1 text-center">
                        <!-- Affichage sous-titre 6 -->
                            <h5 class=" fs-1 text-success m-2"><?php  if ($sous_titre_6) {?> 
                            <?php echo $sous_titre_6; ?>  
                            <?php }?></h5> 
                    </div>
                <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/reduction.svg" class="top mx-auto d-block card-img-with-35" alt="reduction">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 12-->
                                    <p class="card-text"><?php  if ($description_12) {?> 
                                        <?php echo ($description_12); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/technologie.svg" class="top mx-auto d-block card-img-with-35" alt="technologie">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 13 -->
                                    <p class="card-text"><?php  if ($description_13) {?> 
                                        <?php echo ($description_13); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/formation.svg" class="top mx-auto d-block card-img-with-35" alt="formation">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 14 -->
                                    <p class="card-text"><?php  if ($description_14) {?> 
                                        <?php echo nl2br($description_14); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/standardisation.svg" class="top mx-auto d-block card-img-with-35" alt="standardisation">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 15 -->
                                    <p class="card-text"><?php  if ($description_15) {?> 
                                        <?php echo ($description_15); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/entretien.svg" class="top mx-auto d-block card-img-with-35" alt="entretien">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 16 -->
                                    <p class="card-text"><?php  if ($description_16) {?> 
                                        <?php echo nl2br($description_16); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-5">
                <div class="row-cols-1 text-center">
                    <!-- Affichage sous-titre 7 -->
                        <h5 class=" fs-1 text-success m-2"><?php  if ($sous_titre_7) {?> 
                        <?php echo $sous_titre_7; ?>  
                        <?php }?></h5> 
                </div>
                <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 1 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/marche.svg" class="top mx-auto d-block card-img-with-35" alt="marche">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 1 -->
                                    <p class="card-text"><?php  if ($date_8) {?> 
                                        <?php echo ($date_8); ?>  
                                        <?php }?></p>

                                    <p class="card-text"><?php  if ($description_17) {?> 
                                        <?php echo ($description_17); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 2 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/faisabilite.svg" class="top mx-auto d-block card-img-with-35" alt="faisabilite">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 2 -->
                                    <p class="card-text"><?php  if ($date_9) {?> 
                                        <?php echo ($date_9); ?>  
                                        <?php }?></p>
                                    <p class="card-text"><?php  if ($description_18) {?> 
                                        <?php echo ($description_18); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/construction.svg" class="top mx-auto d-block card-img-with-35" alt="construction">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 3 -->
                                    <p class="card-text"><?php  if ($date_10) {?> 
                                        <?php echo ($date_10); ?>  
                                        <?php }?></p>
                                    <p class="card-text"><?php  if ($description_19) {?> 
                                        <?php echo nl2br($description_19); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 4 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/controle.svg" class="top mx-auto d-block card-img-with-35" alt="controle">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 4 -->
                                    <p class="card-text"><?php  if ($date_11) {?> 
                                        <?php echo ($date_11); ?>  
                                        <?php }?></p>
                                    <p class="card-text"><?php  if ($description_20) {?> 
                                        <?php echo nl2br($description_20); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-5">
                <div class="row-cols-1 text-center">
                    <!-- Affichage sous-titre 8 -->
                        <h5 class=" fs-1 text-success m-2"><?php  if ($sous_titre_8) {?> 
                        <?php echo $sous_titre_8; ?>  
                        <?php }?></h5> 
                </div>
                <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/reunion.svg" class="top mx-auto d-block card-img-with-35" alt="reunion">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 21-->
                                    <p class="card-text"><?php  if ($description_21) {?> 
                                        <?php echo ($description_21); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/conception.svg" class="top mx-auto d-block card-img-with-35" alt="conception">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 22 -->
                                    <p class="card-text"><?php  if ($description_22) {?> 
                                        <?php echo ($description_22); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/offre.svg" class="top mx-auto d-block card-img-with-35" alt="offre">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 23 -->
                                    <p class="card-text"><?php  if ($description_23) {?> 
                                        <?php echo nl2br($description_23); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/miseenoeuvre.svg" class="top mx-auto d-block card-img-with-35" alt="miseenoeuvre">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 24 -->
                                    <p class="card-text"><?php  if ($description_24) {?> 
                                        <?php echo ($description_24); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/reunionperiodique.svg" class="top mx-auto d-block card-img-with-35" alt="reunionperiodique">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 25 -->
                                    <p class="card-text"><?php  if ($description_25) {?> 
                                        <?php echo nl2br($description_25); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/miseenmarche.svg" class="top mx-auto d-block card-img-with-35" alt="miseenmarche">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 26 -->
                                    <p class="card-text"><?php  if ($description_26) {?> 
                                        <?php echo nl2br($description_26); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/acceptationmesures.svg" class="top mx-auto d-block card-img-with-35" alt="acceptationmesures">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 27 -->
                                    <p class="card-text"><?php  if ($description_27) {?> 
                                        <?php echo nl2br($description_27); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-5">
                <div class="row-cols-1 text-center">
                    <!-- Affichage sous-titre 9 -->
                        <h5 class=" fs-1 text-success m-2"><?php  if ($sous_titre_9) {?> 
                        <?php echo $sous_titre_9; ?>  
                        <?php }?></h5> 
                </div>
                <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/programme.svg" class="top mx-auto d-block card-img-with-35" alt="programme">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 28-->
                                    <p class="card-text"><?php  if ($description_28) {?> 
                                        <?php echo ($description_28); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/verification.svg" class="top mx-auto d-block card-img-with-35" alt="verification">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 29 -->
                                    <p class="card-text"><?php  if ($description_29) {?> 
                                        <?php echo ($description_29); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="card h-100 text-center" >
                            <!-- Affichage image carte 3 -->
                            <div class="card-img-top ">   
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/finances.svg" class="top mx-auto d-block card-img-with-35" alt="finances">
                            </div>
                            <div class="card-body">
                                <!-- Affichage description carte 30 -->
                                    <p class="card-text"><?php  if ($description_30) {?> 
                                        <?php echo nl2br($description_30); ?>  
                                        <?php }?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
    
<?php get_footer(); ?>