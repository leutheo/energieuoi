<?php
/**
 * Template Name: Valcartier_Ecogestes
 */

?>
<?php get_header(); 
// Caroussel

$caroussel_1 = get_field('caroussel_1');
$picture_caroussel_1 = $caroussel_1['sizes']['my_custom_size_2'];
$caroussel_1_titre = get_field('caroussel_1_titre');
$caroussel_1_description = get_field('caroussel_1_description');

$caroussel_2 = get_field('caroussel_2');
$picture_caroussel_2 = $caroussel_2['sizes']['my_custom_size_2'];
$caroussel_2_titre = get_field('caroussel_2_titre');
$caroussel_2_description = get_field('caroussel_2_description');

$caroussel_3 = get_field('caroussel_3');
$picture_caroussel_3 = $caroussel_3['sizes']['my_custom_size_2'];
$caroussel_3_titre = get_field('caroussel_3_titre');
$caroussel_3_description = get_field('caroussel_3_description');

//Affichage titre 1/
$titre_1 = get_field('titre_1');

//Affichage sous-titre 1/
$sous_titre_1 = get_field('sous_titre_1');

//Affichage sous-titre 2/
$sous_titre_2 = get_field('sous_titre_2');

//Affichage sous-titre 3/
$sous_titre_3 = get_field('sous_titre_3');

$description_1 = get_field('description_1');
$description_2 = get_field('description_2');
$description_3 = get_field('description_3');
$description_4 = get_field('description_4');



//Affichage image carte 2/
$description_picture_2 = get_field('description_picture_2');

//Affichage image carte 3/

$description_picture_3 = get_field('description_picture_3');

//Affichage image carte 4/

$description_picture_4 = get_field('description_picture_4');

//Affichage sous-titre 4/
$sous_titre_4 = get_field('sous_titre_4');

//Affichage sous-titre 5/
$sous_titre_5 = get_field('sous_titre_5');

//Affichage sous-titre 6/
$sous_titre_6 = get_field('sous_titre_6');

$description_5 = get_field('description_5');
$description_6 = get_field('description_6');
$description_7 = get_field('description_7');
$description_8 = get_field('description_8');


//Affichage image 5/

//Affichage image carte 6/
$description_picture_6 = get_field('description_picture_6');

//Affichage image carte 7/
$description_picture_7 = get_field('description_picture_7');

//Affichage image carte 8/
$description_picture_8 = get_field('description_picture_8');


//Affichage sous-titre 7/
$sous_titre_7 = get_field('sous_titre_7');

//Affichage sous-titre 8/
$sous_titre_8 = get_field('sous_titre_8');

//Affichage sous-titre 9/
$sous_titre_9 = get_field('sous_titre_9');

$description_9 = get_field('description_9');
$description_10 = get_field('description_10');
$description_11 = get_field('description_11');
$description_12 = get_field('description_12');


//Affichage image carte 10/
$description_picture_10 = get_field('description_picture_10');

//Affichage image carte 11/
$description_picture_11 = get_field('description_picture_11');

//Affichage image carte 12/
$description_picture_12 = get_field('description_picture_12');



//Affichage sous-titre 10/
$sous_titre_10 = get_field('sous_titre_10');

//Affichage sous-titre 11/
$sous_titre_11 = get_field('sous_titre_11');

//Affichage sous-titre 12/
$sous_titre_12 = get_field('sous_titre_12');

$description_13 = get_field('description_13');
$description_14 = get_field('description_14');
$description_15 = get_field('description_15');
$description_16 = get_field('description_16');


//Affichage image carte 13/
$description_picture_13 = get_field('description_picture_13');

//Affichage image carte 14/
$description_picture_14 = get_field('description_picture_14');

//Affichage image carte 15/
$description_picture_15 = get_field('description_picture_15');


//Affichage sous-titre 13/
$sous_titre_13 = get_field('sous_titre_13');

//Affichage sous-titre 14/
$sous_titre_14 = get_field('sous_titre_14');

//Affichage sous-titre 15/
$sous_titre_15 = get_field('sous_titre_15');

$description_17 = get_field('description_17');
$description_18 = get_field('description_18');
$description_19 = get_field('description_19');
$description_20 = get_field('description_20');


//Affichage image carte 16/
$description_picture_16 = get_field('description_picture_16');

//Affichage image carte 17/
$description_picture_17 = get_field('description_picture_17');

//Affichage image carte 18/
$description_picture_18 = get_field('description_picture_18');


//Affichage sous-titre 16/
$sous_titre_16 = get_field('sous_titre_16');

//Affichage sous-titre 17/
$sous_titre_17 = get_field('sous_titre_17');

//Affichage sous-titre 18/
$sous_titre_18 = get_field('sous_titre_18');

$description_21 = get_field('description_21');
$description_22 = get_field('description_22');
$description_23 = get_field('description_23');
$description_24 = get_field('description_24');


//Affichage image carte 19/
$description_picture_19 = get_field('description_picture_19');

//Affichage image carte 20/
$description_picture_20 = get_field('description_picture_20');

//Affichage image carte 21/
$description_picture_21 = get_field('description_picture_21');


//Affichage sous-titre 19/
$sous_titre_19 = get_field('sous_titre_19');

//Affichage sous-titre 20/
$sous_titre_20 = get_field('sous_titre_20');

//Affichage sous-titre 21/
$sous_titre_21 = get_field('sous_titre_21');

$description_25 = get_field('description_25');
$description_26 = get_field('description_26');
$description_27 = get_field('description_27');
$description_28 = get_field('description_28');


//Affichage image carte 22/
$description_picture_22 = get_field('description_picture_22');

//Affichage image carte 23/
$description_picture_23 = get_field('description_picture_23');

//Affichage image carte 24/
$description_picture_24 = get_field('description_picture_24');
?>

<div class="page-wrap">
    <!-- Affichage de la bannière image -->

    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="<?php echo $picture_caroussel_1 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_1_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_1_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_2 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_2_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_2_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_3 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_3_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_3_description ?></p>
            </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    <main class="container">
        <?php if( have_posts() ) {
            while( have_posts() ) {
                the_post(); ?>
        
                <?php the_content(); ?>
            
            <?php  }
            
            } ?>
        <div class="row">
            <div class="row-cols-1 text-center">
                <!-- Affichage titre 1 -->
                    <h2 class="text-uppercase fs-1 fw-bold text-success m-5 shadow p-3 mb-5 bg-body rounded"><?php  if ($titre_1) {?> 
                    <?php echo $titre_1; ?>  
                    <?php }?></h2> 
            </div>
        </div>
        <div class="accordion" id="accordionExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <div class="row-cols-1 text-center">
                            <!-- Affichage sous-titre 1 -->
                            <h2 class="text-success "><?php  if ($sous_titre_1) {?> 
                            <?php echo $sous_titre_1; ?>  
                            <?php }?></h2> 
                        </div>  
                    </button>
                </h2>
                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-12">
                                <div class="card-body text-center">
                                    <!-- Affichage image carte 1 -->
                                    <div class="text-center"> 
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/interrupteur.svg" class="card-img-with-10" alt="interrupteur">
                                    </div>
                                </div> 
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="row-cols-1 text-center">
                                <!-- Affichage sous-titre 2 -->
                                <h2 class="text-success m-5"><?php  if ($sous_titre_2) {?> 
                                <?php echo $sous_titre_2; ?>  
                                <?php }?></h2> 
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <h5 class="card-title fw-bolder"><?php  if ($description_1) {?> 
                                                <?php echo $description_1; ?>  
                                                <?php }?></h5>
                                            <p class="card-text"><?php  if ($description_2) {?> 
                                                <?php echo $description_2; ?>  
                                                <?php }?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card h-100">
                                <div class="card-body">
                                    <h5 class="card-title fw-bolder"><?php  if ($description_3) {?> 
                                            <?php echo $description_3; ?>  
                                            <?php }?></h5>
                                        <p class="card-text"><?php  if ($description_4) {?> 
                                            <?php echo $description_4; ?>  
                                            <?php }?></p>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row-cols-1 text-center">
                                <!-- Affichage sous-titre 3 -->
                                <h2 class="text-success m-5"><?php  if ($sous_titre_3) {?> 
                                <?php echo $sous_titre_3; ?>  
                                <?php }?></h2> 
                            </div> 
                        </div>
                        <div class="row row-cols-1 row-cols-md-4 g-4">
                            <div class="col-12 col-md-6 col-lg-4 ">
                                <div class="card h-100">
                                    <div class="card-body">   
                                        <!-- Affichage image carte 2 -->
                                        <div class="card-img-top text-center"> 
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fluocompact.png" class="card-img-with-35" alt="fluocompact">
                                        </div>                  
                                            <p class="card-text"><?php  if ($description_picture_2) {?> 
                                            <?php echo nl2br($description_picture_2); ?>  
                                            <?php }?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 ">
                                <div class="card h-100">
                                    <div class="card-body"> 
                                        <!-- Affichage image carte 3 -->
                                        <div class="card-img-top text-center"> 
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ampouleoff.png" class="card-img-with-35" alt="ampouleoff">
                                        </div> 
                                        <p class="card-text"><?php  if ($description_picture_3) {?> 
                                            <?php echo nl2br($description_picture_3); ?>  
                                            <?php }?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <!-- Affichage image carte 4 -->
                                        <div class="card-img-top text-center"> 
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fenetre.png" class="card-img-with-35" alt="fenetre">
                                        </div>
                                        <p class="card-text"><?php  if ($description_picture_4) {?> 
                                            <?php echo nl2br($description_picture_4); ?>  
                                            <?php }?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <div class="row-cols-1 text-center">
                        <!-- Affichage sous-titre 4 -->
                        <h2 class="text-success "><?php  if ($sous_titre_4) {?> 
                        <?php echo $sous_titre_4; ?>  
                        <?php }?></h2> 
                    </div>  
                </button>
                </h2>
                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-12">
                                <div class="card-body text-center">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ordinateur.svg" class="card-img-with-10" alt="ordinateur">
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="row-cols-1 text-center">
                                    <!-- Affichage sous-titre 5 -->
                                    <h2 class="text-success m-5"><?php  if ($sous_titre_5) {?> 
                                    <?php echo $sous_titre_5; ?>  
                                    <?php }?></h2> 
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <h5 class="card-title fw-bolder"><?php  if ($description_5) {?> 
                                                    <?php echo $description_5; ?>  
                                                    <?php }?></h5>
                                                <p class="card-text"><?php  if ($description_6) {?> 
                                                    <?php echo $description_6; ?>  
                                                    <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="card h-100">
                                    <div class="card-body">
                                        <h5 class="card-title fw-bolder"><?php  if ($description_7) {?> 
                                                <?php echo $description_7; ?>  
                                                <?php }?></h5>
                                            <p class="card-text"><?php  if ($description_8) {?> 
                                                <?php echo $description_8; ?>  
                                                <?php }?></p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="row-cols-1 text-center">
                                    <!-- Affichage sous-titre 6 -->
                                    <h2 class="text-success m-5"><?php  if ($sous_titre_6) {?> 
                                    <?php echo $sous_titre_6; ?>  
                                    <?php }?></h2> 
                                </div> 
                            </div>
                            <div class="row row-cols-1 row-cols-md-4 g-4">
                                <div class="col-12 col-md-6 col-lg-4 ">
                                    <div class="card h-100">
                                        <div class="card-body">   
                                            <!-- Affichage image carte 6 -->
                                            <div  class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/verouille.svg" class="card-img-with-35" alt="verouille">
                                            </div>                  
                                                <p class="card-text"><?php  if ($description_picture_6) {?> 
                                                <?php echo nl2br($description_picture_6); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4 ">
                                    <div class="card h-100">
                                        <div class="card-body"> 
                                            <!-- Affichage image carte 7 -->
                                            <div class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/descktop.svg" class="card-img-with-35" alt="descktop">
                                            </div>
                                            <p class="card-text"><?php  if ($description_picture_7) {?> 
                                                <?php echo nl2br($description_picture_7); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <!-- Affichage image carte 8 -->
                                            <div class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/eteint.svg" class="card-img-with-35" alt="eteint">
                                            </div>
                                            <p class="card-text"><?php  if ($description_picture_8) {?> 
                                                <?php echo nl2br($description_picture_8); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingThree">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <div class="row-cols-1 text-center">
                        <!-- Affichage sous-titre 7 -->
                        <h2 class="text-success "><?php  if ($sous_titre_7) {?> 
                        <?php echo $sous_titre_7; ?>  
                        <?php }?></h2> 
                    </div> 
                </button>
                </h2>
                <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <div class="row">
                            <div class="card-img-top text-center">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinet.svg" class="card-img-with-10" alt="robinet">                                
                            </div>
                            
                            <div class="row">
                                <div class="row-cols-1 text-center">
                                    <!-- Affichage sous-titre 8 -->
                                    <h2 class="text-success m-5"><?php  if ($sous_titre_8) {?> 
                                    <?php echo $sous_titre_8; ?>  
                                    <?php }?></h2> 
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <h5 class="card-title fw-bolder"><?php  if ($description_9) {?> 
                                                    <?php echo $description_9; ?>  
                                                    <?php }?></h5>
                                                <p class="card-text"><?php  if ($description_10) {?> 
                                                    <?php echo $description_10; ?>  
                                                    <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="card h-100">
                                    <div class="card-body">
                                        <h5 class="card-title fw-bolder"><?php  if ($description_11) {?> 
                                                <?php echo $description_11; ?>  
                                                <?php }?></h5>
                                            <p class="card-text"><?php  if ($description_12) {?> 
                                                <?php echo $description_12; ?>  
                                                <?php }?></p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="row-cols-1 text-center">
                                    <!-- Affichage sous-titre 9 -->
                                    <h2 class="text-success m-5"><?php  if ($sous_titre_9) {?> 
                                    <?php echo $sous_titre_9; ?>  
                                    <?php }?></h2> 
                                </div> 
                            </div>
                            <div class="row row-cols-1 row-cols-md-4 g-4">
                                <div class="col-12 col-md-6 col-lg-4 ">
                                    <div class="card h-100">
                                        <div class="card-body">   
                                            <!-- Affichage image carte 10 -->
                                            <div  class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/feu.svg" class="card-img-with-35" alt="feu">
                                            </div>                  
                                                <p class="card-text"><?php  if ($description_picture_10) {?> 
                                                <?php echo nl2br($description_picture_10); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4 ">
                                    <div class="card h-100">
                                        <div class="card-body"> 
                                            <!-- Affichage image carte 11 -->
                                            <div class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinetchaud.svg" class="card-img-with-35" alt="robinetchaud"> 
                                            </div>
                                            <p class="card-text"><?php  if ($description_picture_11) {?> 
                                                <?php echo nl2br($description_picture_11); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <!-- Affichage image carte 12 -->
                                            <div class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinet1.svg" class="card-img-with-35" alt="robinet1">
                                            </div>
                                            <p class="card-text"><?php  if ($description_picture_12) {?> 
                                                <?php echo nl2br($description_picture_12); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingFour">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                    <div class="row-cols-1 text-center">
                        <!-- Affichage sous-titre 10 -->
                        <h2 class="text-success "><?php  if ($sous_titre_10) {?> 
                        <?php echo $sous_titre_10; ?>  
                        <?php }?></h2> 
                    </div> 
                </button>
                </h2>
                <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <div class="row">
                            <div class="card-img-top text-center">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinet.svg" class="card-img-with-10" alt="robinet">                                
                            </div>
                            
                            <div class="row">
                                <div class="row-cols-1 text-center">
                                    <!-- Affichage sous-titre 11 -->
                                    <h2 class="text-success m-5"><?php  if ($sous_titre_11) {?> 
                                    <?php echo $sous_titre_11; ?>  
                                    <?php }?></h2> 
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <h5 class="card-title fw-bolder"><?php  if ($description_13) {?> 
                                                    <?php echo $description_13; ?>  
                                                    <?php }?></h5>
                                                <p class="card-text"><?php  if ($description_14) {?> 
                                                    <?php echo $description_14; ?>  
                                                    <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="card h-100">
                                    <div class="card-body">
                                        <h5 class="card-title fw-bolder"><?php  if ($description_15) {?> 
                                                <?php echo $description_15; ?>  
                                                <?php }?></h5>
                                            <p class="card-text"><?php  if ($description_16) {?> 
                                                <?php echo $description_16; ?>  
                                                <?php }?></p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="row-cols-1 text-center">
                                    <!-- Affichage sous-titre 12 -->
                                    <h2 class="text-success m-5"><?php  if ($sous_titre_12) {?> 
                                    <?php echo $sous_titre_12; ?>  
                                    <?php }?></h2> 
                                </div> 
                            </div>
                            <div class="row row-cols-1 row-cols-md-4 g-4">
                                <div class="col-12 col-md-6 col-lg-4 ">
                                    <div class="card h-100">
                                        <div class="card-body">   
                                            <!-- Affichage image carte 13 -->
                                            <div  class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/feu.svg" class="card-img-with-35" alt="feu">
                                            </div>                  
                                                <p class="card-text"><?php  if ($description_picture_13) {?> 
                                                <?php echo nl2br($description_picture_13); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4 ">
                                    <div class="card h-100">
                                        <div class="card-body"> 
                                            <!-- Affichage image carte 14 -->
                                            <div class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinetchaud.svg" class="card-img-with-35" alt="robinetchaud"> 
                                            </div>
                                            <p class="card-text"><?php  if ($description_picture_14) {?> 
                                                <?php echo nl2br($description_picture_14); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <!-- Affichage image carte 15 -->
                                            <div class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinet1.svg" class="card-img-with-35" alt="robinet1">
                                            </div>
                                            <p class="card-text"><?php  if ($description_picture_15) {?> 
                                                <?php echo nl2br($description_picture_15); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingFive">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                    <div class="row-cols-1 text-center">
                        <!-- Affichage sous-titre 13 -->
                        <h2 class="text-success "><?php  if ($sous_titre_13) {?> 
                        <?php echo $sous_titre_13; ?>  
                        <?php }?></h2> 
                    </div> 
                </button>
                </h2>
                <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <div class="row">
                            <div class="card-img-top text-center">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinet.svg" class="card-img-with-10" alt="robinet">                                
                            </div>
                            
                            <div class="row">
                                <div class="row-cols-1 text-center">
                                    <!-- Affichage sous-titre 14 -->
                                    <h2 class="text-success m-5"><?php  if ($sous_titre_14) {?> 
                                    <?php echo $sous_titre_14; ?>  
                                    <?php }?></h2> 
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <h5 class="card-title fw-bolder"><?php  if ($description_17) {?> 
                                                    <?php echo $description_17; ?>  
                                                    <?php }?></h5>
                                                <p class="card-text"><?php  if ($description_18) {?> 
                                                    <?php echo $description_18; ?>  
                                                    <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="card h-100">
                                    <div class="card-body">
                                        <h5 class="card-title fw-bolder"><?php  if ($description_19) {?> 
                                                <?php echo $description_19; ?>  
                                                <?php }?></h5>
                                            <p class="card-text"><?php  if ($description_20) {?> 
                                                <?php echo $description_20; ?>  
                                                <?php }?></p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="row-cols-1 text-center">
                                    <!-- Affichage sous-titre 15 -->
                                    <h2 class="text-success m-5"><?php  if ($sous_titre_15) {?> 
                                    <?php echo $sous_titre_15; ?>  
                                    <?php }?></h2> 
                                </div> 
                            </div>
                            <div class="row row-cols-1 row-cols-md-4 g-4">
                                <div class="col-12 col-md-6 col-lg-4 ">
                                    <div class="card h-100">
                                        <div class="card-body">   
                                            <!-- Affichage image carte 16 -->
                                            <div  class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/feu.svg" class="card-img-with-35" alt="feu">
                                            </div>                  
                                                <p class="card-text"><?php  if ($description_picture_16) {?> 
                                                <?php echo nl2br($description_picture_16); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4 ">
                                    <div class="card h-100">
                                        <div class="card-body"> 
                                            <!-- Affichage image carte 17 -->
                                            <div class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinetchaud.svg" class="card-img-with-35" alt="robinetchaud"> 
                                            </div>
                                            <p class="card-text"><?php  if ($description_picture_17) {?> 
                                                <?php echo nl2br($description_picture_17); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <!-- Affichage image carte 18 -->
                                            <div class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinet1.svg" class="card-img-with-35" alt="robinet1">
                                            </div>
                                            <p class="card-text"><?php  if ($description_picture_18) {?> 
                                                <?php echo nl2br($description_picture_18); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingSix">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                    <div class="row-cols-1 text-center">
                        <!-- Affichage sous-titre 16 -->
                        <h2 class="text-success "><?php  if ($sous_titre_16) {?> 
                        <?php echo $sous_titre_16; ?>  
                        <?php }?></h2> 
                    </div> 
                </button>
                </h2>
                <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <div class="row">
                            <div class="card-img-top text-center">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinet.svg" class="card-img-with-10" alt="robinet">                                
                            </div>
                            
                            <div class="row">
                                <div class="row-cols-1 text-center">
                                    <!-- Affichage sous-titre 17 -->
                                    <h2 class="text-success m-5"><?php  if ($sous_titre_17) {?> 
                                    <?php echo $sous_titre_17; ?>  
                                    <?php }?></h2> 
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <h5 class="card-title fw-bolder"><?php  if ($description_21) {?> 
                                                    <?php echo $description_21; ?>  
                                                    <?php }?></h5>
                                                <p class="card-text"><?php  if ($description_22) {?> 
                                                    <?php echo $description_22; ?>  
                                                    <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="card h-100">
                                    <div class="card-body">
                                        <h5 class="card-title fw-bolder"><?php  if ($description_23) {?> 
                                                <?php echo $description_23; ?>  
                                                <?php }?></h5>
                                            <p class="card-text"><?php  if ($description_24) {?> 
                                                <?php echo $description_24; ?>  
                                                <?php }?></p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="row-cols-1 text-center">
                                    <!-- Affichage sous-titre 18 -->
                                    <h2 class="text-success m-5"><?php  if ($sous_titre_18) {?> 
                                    <?php echo $sous_titre_18; ?>  
                                    <?php }?></h2> 
                                </div> 
                            </div>
                            <div class="row row-cols-1 row-cols-md-4 g-4">
                                <div class="col-12 col-md-6 col-lg-4 ">
                                    <div class="card h-100">
                                        <div class="card-body">   
                                            <!-- Affichage image carte 19 -->
                                            <div  class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/feu.svg" class="card-img-with-35" alt="feu">
                                            </div>                  
                                                <p class="card-text"><?php  if ($description_picture_19) {?> 
                                                <?php echo nl2br($description_picture_19); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4 ">
                                    <div class="card h-100">
                                        <div class="card-body"> 
                                            <!-- Affichage image carte 20 -->
                                            <div class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinetchaud.svg" class="card-img-with-35" alt="robinetchaud"> 
                                            </div>
                                            <p class="card-text"><?php  if ($description_picture_20) {?> 
                                                <?php echo nl2br($description_picture_20); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <!-- Affichage image carte 21 -->
                                            <div class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinet1.svg" class="card-img-with-35" alt="robinet1">
                                            </div>
                                            <p class="card-text"><?php  if ($description_picture_21) {?> 
                                                <?php echo nl2br($description_picture_21); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingSevent">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSevent" aria-expanded="false" aria-controls="collapseSevent">
                    <div class="row-cols-1 text-center">
                        <!-- Affichage sous-titre 19 -->
                        <h2 class="text-success "><?php  if ($sous_titre_19) {?> 
                        <?php echo $sous_titre_19; ?>  
                        <?php }?></h2> 
                    </div> 
                </button>
                </h2>
                <div id="collapseSevent" class="accordion-collapse collapse" aria-labelledby="headingSevent" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <div class="row">
                            <div class="card-img-top text-center">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinet.svg" class="card-img-with-10" alt="robinet">                                
                            </div>
                            <div class="row">
                                <div class="row-cols-1 text-center">
                                    <!-- Affichage sous-titre 20 -->
                                    <h2 class="text-success m-5"><?php  if ($sous_titre_20) {?> 
                                    <?php echo $sous_titre_20; ?>  
                                    <?php }?></h2> 
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <h5 class="card-title fw-bolder"><?php  if ($description_25) {?> 
                                                    <?php echo $description_25; ?>  
                                                    <?php }?></h5>
                                                <p class="card-text"><?php  if ($description_26) {?> 
                                                    <?php echo $description_26; ?>  
                                                    <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="card h-100">
                                    <div class="card-body">
                                        <h5 class="card-title fw-bolder"><?php  if ($description_27) {?> 
                                                <?php echo $description_27; ?>  
                                                <?php }?></h5>
                                            <p class="card-text"><?php  if ($description_28) {?> 
                                                <?php echo $description_28; ?>  
                                                <?php }?></p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="row-cols-1 text-center">
                                    <!-- Affichage sous-titre 21 -->
                                    <h2 class="text-success m-5"><?php  if ($sous_titre_21) {?> 
                                    <?php echo $sous_titre_21; ?>  
                                    <?php }?></h2> 
                                </div> 
                            </div>
                            <div class="row row-cols-1 row-cols-md-4 g-4">
                                <div class="col-12 col-md-6 col-lg-4 ">
                                    <div class="card h-100">
                                        <div class="card-body">   
                                            <!-- Affichage image carte 22 -->
                                            <div  class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/feu.svg" class="card-img-with-35" alt="feu">
                                            </div>                  
                                                <p class="card-text"><?php  if ($description_picture_22) {?> 
                                                <?php echo nl2br($description_picture_22); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4 ">
                                    <div class="card h-100">
                                        <div class="card-body"> 
                                            <!-- Affichage image carte 23 -->
                                            <div class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinetchaud.svg" class="card-img-with-35" alt="robinetchaud"> 
                                            </div>
                                            <p class="card-text"><?php  if ($description_picture_23) {?> 
                                                <?php echo nl2br($description_picture_23); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <!-- Affichage image carte 24 -->
                                            <div class="card-img-top text-center">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/robinet1.svg" class="card-img-with-35" alt="robinet1">
                                            </div>
                                            <p class="card-text"><?php  if ($description_picture_24) {?> 
                                                <?php echo nl2br($description_picture_24); ?>  
                                                <?php }?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </main>
</div>
    
<?php get_footer(); ?>