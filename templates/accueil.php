<?php
/**
 * Template Name: Accueil
 */
?>
<?php get_header(); 

$image_id = get_field('image_page');

// Caroussel

$caroussel_1 = get_field('caroussel_1');
$picture_caroussel_1 = $caroussel_1['sizes']['my_custom_size_2'];
$caroussel_1_titre = get_field('caroussel_1_titre');
$caroussel_1_description = get_field('caroussel_1_description');

$caroussel_2 = get_field('caroussel_2');
$picture_caroussel_2 = $caroussel_2['sizes']['my_custom_size_2'];
$caroussel_2_titre = get_field('caroussel_2_titre');
$caroussel_2_description = get_field('caroussel_2_description');

$caroussel_3 = get_field('caroussel_3');
$picture_caroussel_3 = $caroussel_3['sizes']['my_custom_size_2'];
$caroussel_3_titre = get_field('caroussel_3_titre');
$caroussel_3_description = get_field('caroussel_3_description');

//Affichage carte 1/
$sous_titre_1 = get_field('sous_titre_1');

//Affichage carte 1/
$titre_carte_1 = get_field('titre_carte_1');
$image_carte_1 = get_field('image_carte_1');
$picture_1 = $image_carte_1['sizes']['my_custom_size_1'];
$description_carte_1 = get_field('description_carte_1');

//Affichage carte 2/
$titre_carte_2 = get_field('titre_carte_2');
$image_carte_2 = get_field('image_carte_2');
$picture_2 = $image_carte_2['sizes']['my_custom_size_1'];
$description_carte_2 = get_field('description_carte_2');

//Affichage carte 3/
$titre_carte_3 = get_field('titre_carte_3');
$image_carte_3 = get_field('image_carte_3');
$picture_3 = $image_carte_3['sizes']['my_custom_size_1'];
$description_carte_3 = get_field('description_carte_3');

//Affichage carte 2/
$sous_titre_2 = get_field('sous_titre_2');

//Affichage carte 4/
$titre_carte_4 = get_field('titre_carte_4');
$image_carte_4 = get_field('image_carte_4');
$picture_4 = $image_carte_4['sizes']['my_custom_size_1'];
$description_carte_4 = get_field('description_carte_4');

//Affichage carte 5/
$titre_carte_5 = get_field('titre_carte_5');
$image_carte_5 = get_field('image_carte_5');
$picture_5 = $image_carte_5['sizes']['my_custom_size_1'];
$description_carte_5 = get_field('description_carte_5');

//Affichage carte 6/
$titre_carte_6 = get_field('titre_carte_6');
$image_carte_6 = get_field('image_carte_6');
$picture_6 = $image_carte_6['sizes']['my_custom_size_1'];
$description_carte_6 = get_field('description_carte_6');

//Affichage carte 7/
$titre_carte_7 = get_field('titre_carte_7');
$image_carte_7 = get_field('image_carte_7');
$picture_7 = $image_carte_7['sizes']['my_custom_size_1'];
$description_carte_7 = get_field('description_carte_7');

//Affichage sous-titre 3/
$sous_titre_3 = get_field('sous_titre_3');
$description_sous_titre_3 = get_field('description_sous_titre_3');
$image_sous_titre_3 = get_field('image_sous_titre_3');
$picture_sous_titre_3 = $image_sous_titre_3['sizes']['my_custom_size_1'];

//Affichage sous-titre 4/
$sous_titre_4 = get_field('sous_titre_4');
$titre_carte_1_sous_tritre_4 = get_field('titre_carte_1_sous_tritre_4');
$image_carte_1_sous_tritre_4 = get_field('image_carte_1_sous_tritre_4');
$picture_image_carte_1_sous_tritre_4 = $image_carte_1_sous_tritre_4['sizes']['my_custom_size_1'];

$titre_carte_2_sous_tritre_4 = get_field('titre_carte_2_sous_tritre_4');
$image_carte_2_sous_tritre_4 = get_field('image_carte_2_sous_tritre_4');
$picture_image_carte_2_sous_tritre_4 = $image_carte_2_sous_tritre_4['sizes']['my_custom_size_1'];

$titre_carte_3_sous_tritre_4 = get_field('titre_carte_3_sous_tritre_4');
$image_carte_3_sous_tritre_4 = get_field('image_carte_3_sous_tritre_4');
$picture_image_carte_3_sous_tritre_4 = $image_carte_3_sous_tritre_4['sizes']['my_custom_size_1'];

?>

<div class="page-wrap">
    <!-- Affichage de la bannière image -->

    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="<?php echo $picture_caroussel_1 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_1_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_1_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_2 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_2_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_2_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_3 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_3_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_3_description ?></p>
            </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <main class="container">
        <?php if( have_posts() ) {
            while( have_posts() ) {
                the_post(); ?>
                <?php the_content(); ?>
            
            <?php  }
            
            } ?>
    
        
            <div class="row">
                <div class="row-cols-1 text-center">
                    <!-- Affichage sous-titre 1 -->
                        <h2 class="text-success m-5 shadow p-3 mb-5 bg-body rounded"><?php  if ($sous_titre_1) {?> 
                        <?php echo $sous_titre_1; ?>  
                        <?php }?></h2> 
                </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 g-4">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100" >
                        <!-- Affichage image carte 1 -->
                            <img src="<?php echo $picture_1 ?>" class="card-img-top" alt="...">
                        <div class="card-body">
                            <!-- Affichage en-tête carte 1 -->
                                <h5 class="card-title e-title"><?php  if ($titre_carte_1) {?> 
                                    <?php echo $titre_carte_1; ?>  
                                <?php }?></h5>

                            <!-- Affichage description carte 1 -->
                                <p class="card-text"><?php  if ($description_carte_1) {?> 
                                    <?php echo nl2br($description_carte_1); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100">
                        <!-- Affichage image carte 2 --> 
                        <img src="<?php echo $picture_2 ?>" class="card-img-top" alt="...">
                        
                        <div class="card-body">
                            <!-- Affichage en-tête carte 2 -->
                                <h5 class="card-title"><?php  if ($titre_carte_2) {?> 
                                    <?php echo $titre_carte_2; ?>  
                                    <?php }?></h5> 

                            <!-- Affichage description carte 2 -->
                                <p class="card-text"><?php  if ($description_carte_2) {?> 
                                    <?php echo nl2br($description_carte_2); ?>  
                                    <?php }?></p>
                        </div> 
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100">
                        <!-- Affichage image carte 3 --> 
                        <img src="<?php echo $picture_3 ?>" class="card-img-top" alt="...">
                        
                        <div class="card-body">
                            <!-- Affichage en-tête carte 3 -->
                                <h5 class="card-title"><?php  if ($titre_carte_3) {?> 
                                    <?php echo $titre_carte_3; ?>  
                                    <?php }?></h5> 

                            <!-- Affichage description carte 3 -->
                                <p class="card-text"><?php  if ($description_carte_3) {?> 
                                    <?php echo nl2br($description_carte_3); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>   
            </div>
            <div class="row">
                <div class="row-cols-1 text-center">
                    <!-- Affichage sous-titre 2 -->
                        <h2 class="text-success m-5 shadow p-3 mb-5 bg-body rounded"><?php  if ($sous_titre_2) {?> 
                        <?php echo $sous_titre_2; ?>  
                        <?php }?></h2> 
                </div>
            </div>
            <div class="row row-cols-1 row-cols-md-4 g-4">
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="card h-100">
                    <!-- Affichage image carte 4 --> 
                        <img src="<?php echo $picture_4 ?>" class="card-img-top" alt="...">

                        <div class="card-body">
                            <!-- Affichage en-tête carte 4 -->
                                <h5 class="card-title"><?php  if ($titre_carte_4) {?> 
                                <?php echo $titre_carte_4; ?>  
                                <?php }?></h5> 

                            <!-- Affichage description carte 4 -->
                                <p class="card-text"><?php  if ($description_carte_4) {?> 
                                    <?php echo nl2br($description_carte_4); ?>  
                                    <?php }?></p>
                        </div>        
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="card h-100">
                        <!-- Affichage image carte 5 -->    
                        <img src="<?php echo $picture_5 ?>" class="card-img-top" alt="...">
                            
                        <div class="card-body">
                            <!-- Affichage en-tête carte 5 -->
                                <h5 class="card-title"><?php  if ($titre_carte_5) {?> 
                                <?php echo $titre_carte_5; ?>  
                                <?php }?></h5> 

                            <!-- Affichage description carte 5 -->
                                <p class="card-text"><?php  if ($description_carte_5) {?> 
                                    <?php echo nl2br($description_carte_5); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="card h-100">
                         <!-- Affichage image carte 6 -->
                         <img src="<?php echo $picture_6 ?>" class="card-img-top" alt="...">
                        
                        <div class="card-body">
                            <!-- Affichage en-tête carte 6 -->
                                <h5 class="card-title"><?php  if ($titre_carte_6) {?> 
                                <?php echo $titre_carte_6; ?>  
                                <?php }?></h5> 

                            <!-- Affichage description carte 6 -->
                                <p class="card-text"><?php  if ($description_carte_6) {?> 
                                    <?php echo nl2br($description_carte_6); ?>  
                                    <?php }?></p>
                        </div>
                    </div> 
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="card h-100">
                        <!-- Affichage image carte 7 -->    
                        <img src="<?php echo $picture_7 ?>" class="card-img-top" alt="...">
                       
                        <div class="card-body">
                             <!-- Affichage en-tête carte 7 -->
                                <h5 class="card-title"><?php  if ($titre_carte_7) {?> 
                                <?php echo $titre_carte_7; ?>  
                                <?php }?></h5> 
                                
                            <!-- Affichage description carte 7 -->
                                <p class="card-text"><?php  if ($description_carte_7) {?> 
                                    <?php echo nl2br($description_carte_7); ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row row-cols-1 text-center">
                    <!-- Affichage sous-titre 3 -->
                        <h2 class="text-success m-5 shadow p-3 mb-5 bg-body rounded"><?php  if ($sous_titre_3) {?> 
                        <?php echo $sous_titre_3; ?>  
                        <?php }?></h2> 
                </div>
            </div>
            <div class="row row-cols-1 row-cols-md-2 g-4">
                <div class="col-12 col-md-12 col-lg-6">
                    <div class="card h-100">
                        <div class="card-body">
                            <!-- Affichage description sous-titre 3 -->
                                <span class="card-text"><?php  if ($description_sous_titre_3) {?> 
                                <?php echo nl2br($description_sous_titre_3); ?>  
                                <?php }?></span>
                        </div>
                    </div>
                </div>
                
                <div class="col-12 col-md-12 col-lg-6">
                    <div class="card h-100">
                        <!-- Affichage image sous-titre 3 -->    
                        <img src="<?php echo $picture_sous_titre_3 ?>" class="card-img-top" alt="...">
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="row row-cols-1 text-center">
                    <!-- Affichage sous-titre 4 -->
                        <h2 class="text-success m-5 shadow p-3 mb-5 bg-body rounded"><?php  if ($sous_titre_4) {?> 
                        <?php echo $sous_titre_4; ?>  
                        <?php }?></h2> 
                </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 g-4">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100">
                        <div class="card-body text-center test">
                            <!-- Affichage titre 1 sous titre 4 -->
                            <h5 class="card-title"><?php  if ($titre_carte_1_sous_tritre_4) {?> 
                                <?php echo $titre_carte_1_sous_tritre_4; ?>  
                            <?php }?></h5> 
                        </div>
                            <!-- Affichage iamge 1 sous titre 4 -->
                            <img src="<?php echo $picture_image_carte_1_sous_tritre_4 ?>" class="card-img-top" alt="...">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100">
                        <div class="card-body text-center test">
                            <!-- Affichage titre 2 sous titre 4 -->
                                <h5 class="card-title"><?php  if ($titre_carte_2_sous_tritre_4) {?> 
                                    <?php echo $titre_carte_2_sous_tritre_4; ?>  
                                <?php }?></h5>
                        </div>
                            <!-- Affichage iamge 2 sous titre 4 -->
                            <img src="<?php echo $picture_image_carte_2_sous_tritre_4 ?>" class="card-img-top" alt="...">                        
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100">
                        <div class="card-body text-center test">
                            <!-- Affichage titre 3 sous titre 4 -->
                                <h5 class="card-title"><?php  if ($titre_carte_3_sous_tritre_4) {?> 
                                    <?php echo $titre_carte_3_sous_tritre_4; ?>  
                                <?php }?></h5> 
                        </div>
                            <!-- Affichage iamge 3 sous titre 4 -->
                            <img src="<?php echo $picture_image_carte_3_sous_tritre_4 ?>" class="card-img-top" alt="...">
                    </div>
                </div>
            </div>
        </div>        
    </main>
</div>
<?php get_footer(); ?>