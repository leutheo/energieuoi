<?php
/**
 * Template Name: Bagotville_Mesures_Implantees
 */

?>
<?php get_header(); 

// Caroussel

$caroussel_1 = get_field('caroussel_1');
$picture_caroussel_1 = $caroussel_1['sizes']['my_custom_size_2'];
$caroussel_1_titre = get_field('caroussel_1_titre');
$caroussel_1_description = get_field('caroussel_1_description');

$caroussel_2 = get_field('caroussel_2');
$picture_caroussel_2 = $caroussel_2['sizes']['my_custom_size_2'];
$caroussel_2_titre = get_field('caroussel_2_titre');
$caroussel_2_description = get_field('caroussel_2_description');

$caroussel_3 = get_field('caroussel_3');
$picture_caroussel_3 = $caroussel_3['sizes']['my_custom_size_2'];
$caroussel_3_titre = get_field('caroussel_3_titre');
$caroussel_3_description = get_field('caroussel_3_description');

//Affichage carte 1/
$titre_1 = get_field('titre_1');

//Affichage carte 1/
$description_carte_1 = get_field('description_carte_1');

//Affichage carte 2/
$description_carte_2 = get_field('description_carte_2');

//Affichage carte 3/
$description_carte_3 = get_field('description_carte_3');

//Affichage carte 4/
$description_carte_4 = get_field('description_carte_4');

//Affichage carte 5/
$description_carte_5 = get_field('description_carte_5');

//Affichage carte 6/
$description_carte_6 = get_field('description_carte_6');

//Affichage carte 7/
$description_carte_7 = get_field('description_carte_7');

?>

<div class="page-wrap">

    <!-- Affichage de la bannière image -->

    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="<?php echo $picture_caroussel_1 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_1_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_1_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_2 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_2_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_2_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_3 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_3_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_3_description ?></p>
            </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    
    <main class="container">
        <?php if( have_posts() ) {
            while( have_posts() ) {
                the_post(); ?>
        
                <?php the_content(); ?>
            
            <?php  }
            
            } ?>

        <div class="container">
            <div class="row">
                <div class="row-cols-1 text-center">
                    <!-- Affichage titre 1 -->
                        <h2 class=" text-uppercase fs-1 fw-bold text-success m-5 shadow p-3 mb-5 bg-body rounded"><?php  if ($titre_1) {?> 
                        <?php echo $titre_1; ?>  
                        <?php }?></h2> 
                </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100 text-center text-uppercase" >
                        <div class="card-body">
                            <!-- Affichage description carte 1 -->
                                <p class="card-text"><?php  if ($description_carte_1) {?> 
                                    <?php echo nl2br($description_carte_1); ?>  
                                    <?php }?>
                                </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100 text-center text-uppercase">
                        <div class="card-body">
                            <!-- Affichage description carte 2 -->
                                <p class="card-text"><?php  if ($description_carte_2) {?> 
                                    <?php echo nl2br($description_carte_2); ?>  
                                    <?php }?>
                                </p>
                        </div> 
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100 text-center text-uppercase">
                        <div class="card-body">
                            <!-- Affichage description carte 3 -->
                                <p class="card-text"><?php  if ($description_carte_3) {?> 
                                    <?php echo nl2br($description_carte_3); ?>  
                                    <?php }?>
                                </p>
                        </div>
                    </div>
                </div>   
            
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100 text-center text-uppercase" >
                        <div class="card-body">
                            <!-- Affichage description carte 4 -->
                                <p class="card-text"><?php  if ($description_carte_4) {?> 
                                    <?php echo nl2br($description_carte_4); ?>  
                                    <?php }?>
                                </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100 text-center text-uppercase">
                        <div class="card-body">
                            <!-- Affichage description carte 5 -->
                                <p class="card-text"><?php  if ($description_carte_5) {?> 
                                    <?php echo nl2br($description_carte_5); ?>  
                                    <?php }?>
                                </p>
                        </div> 
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100 text-center text-uppercase">
                        <div class="card-body">
                            <!-- Affichage description carte 6 -->
                                <p class="card-text"><?php  if ($description_carte_6) {?> 
                                    <?php echo nl2br($description_carte_6); ?>  
                                    <?php }?>
                                </p>
                        </div>
                    </div>
                </div>   
            
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100 text-center text-uppercase">
                        <div class="card-body">
                            <!-- Affichage description carte 7 -->
                                <p class="card-text"><?php  if ($description_carte_7) {?> 
                                    <?php echo nl2br($description_carte_7); ?>  
                                    <?php }?>
                                </p>
                        </div> 
                    </div>
                </div> 
            </div>
        </div>
    </main>
</div>
    
<?php get_footer(); ?>