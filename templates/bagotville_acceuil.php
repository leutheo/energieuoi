<?php
/**
 * Template Name: Bagotville_Accueil
 */
?>
<?php get_header(); 

// Caroussel

$caroussel_1 = get_field('caroussel_1');
$picture_caroussel_1 = $caroussel_1['sizes']['my_custom_size_2'];
$caroussel_1_titre = get_field('caroussel_1_titre');
$caroussel_1_description = get_field('caroussel_1_description');

$caroussel_2 = get_field('caroussel_2');
$picture_caroussel_2 = $caroussel_2['sizes']['my_custom_size_2'];
$caroussel_2_titre = get_field('caroussel_2_titre');
$caroussel_2_description = get_field('caroussel_2_description');

$caroussel_3 = get_field('caroussel_3');
$picture_caroussel_3 = $caroussel_3['sizes']['my_custom_size_2'];
$caroussel_3_titre = get_field('caroussel_3_titre');
$caroussel_3_description = get_field('caroussel_3_description');

//Affichage de l'introduction
$introduction = get_field('introduction');

//Affichage carte 1/
$titre_1 = get_field('titre_1');

//Affichage carte 1/
$titre_carte_1 = get_field('titre_carte_1');
$description_carte_1 = get_field('description_carte_1');

//Affichage carte 2/
$titre_carte_2 = get_field('titre_carte_2');
$description_carte_2 = get_field('description_carte_2');

//Affichage carte 3/
$titre_carte_3 = get_field('titre_carte_3');
$description_carte_3 = get_field('description_carte_3');

//Affichage carte 4/
$titre_carte_4 = get_field('titre_carte_4');
$description_carte_4 = get_field('description_carte_4');

//Affichage carte 5/
$titre_carte_5 = get_field('titre_carte_5');
$description_carte_5 = get_field('description_carte_5');

?>

<div class="page-wrap">
    <!-- Affichage de la bannière image -->

    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="<?php echo $picture_caroussel_1 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_1_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_1_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_2 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_2_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_2_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_3 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_3_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_3_description ?></p>
            </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <main class="container">
        <?php if( have_posts() ) {
            while( have_posts() ) {
                the_post(); ?>

                <?php the_content(); ?>
            
            <?php  }
            
            } ?>
        
        <div class="container">
            <div class="row">
                <div class="row-cols-1 text-center">
                    <!-- Affichage titre 1 -->
                        <h2 class="text-uppercase fs-1 fw-bold text-success m-5 shadow p-3 mb-5 bg-body rounded"><?php  if ($titre_1) {?> 
                        <?php echo $titre_1; ?>  
                        <?php }?></h2> 
                </div>
            </div>
            <div class="row">
                <div class="row-cols-1">
                    <!-- Affichage introduction -->
                        <p class="card-text"><?php  if ($introduction) {?> 
                            <?php echo ($introduction); ?>  
                            <?php }?></p>
                </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100" >
                        <div class="card-body text-center">
                            <!-- Affichage en-tête carte 1 -->
                                <h5 class="card-title "><?php  if ($titre_carte_1) {?> 
                                    <?php echo $titre_carte_1; ?>  
                                    <?php }?></h5>
                            <!-- Affichage image carte 1 -->
                                <div class="card-img-top text-center"> 
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/building.svg" class="card-img-with-35" alt="energy">
                                </div>
                            <!-- Affichage description carte 1 -->
                                <h6 class="card-text "><?php  if ($description_carte_1) {?> 
                                    <?php echo ($description_carte_1); ?>  
                                    <?php }?></h6>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100">
                        <div class="card-body text-center">
                            <!-- Affichage en-tête carte 2 -->
                                <h5 class="card-title "><?php  if ($titre_carte_2) {?> 
                                    <?php echo $titre_carte_2; ?>  
                                    <?php }?></h5> 
                            <!-- Affichage image carte 2 --> 
                            <div class="card-img-top text-center"> 
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/investissement.svg" class="card-img-with-35" alt="energy">
                            </div>
                            <!-- Affichage description carte 2 -->
                                <h6 class="card-text "><?php  if ($description_carte_2) {?> 
                                    <?php echo ($description_carte_2); ?>  
                                    <?php }?></h6>
                        </div> 
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100">
                        <div class="card-body text-center">
                            <!-- Affichage en-tête carte 3 -->
                                <h5 class="card-title "><?php  if ($titre_carte_3) {?> 
                                    <?php echo $titre_carte_3; ?>  
                                    <?php }?></h5> 
                            <!-- Affichage image carte 3 --> 
                            <div class="card-img-top">   
                                <div class="card-img-top text-center"> 
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/economieenergie.svg" class="card-img-with-35" alt="energy">
                                </div>
                            </div>
                            <!-- Affichage description carte 3 -->
                                <h6 class="card-text "><?php  if ($description_carte_3) {?> 
                                    <?php echo ($description_carte_3); ?>  
                                    <?php }?></h6>
                        </div>
                    </div>
                </div>   
            </div>
            <div class="row row-cols-1 row-cols-md-2 g-4 mt-3">
            <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100">
                        <div class="card-body text-center">
                            <!-- Affichage en-tête carte 4 -->
                                <h5 class="card-title "><?php  if ($titre_carte_4) {?> 
                                    <?php echo $titre_carte_4; ?>  
                                    <?php }?></h5> 
                            <!-- Affichage image carte 4 --> 
                                <div class="card-img-top text-center"> 
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gazserre.svg" class="card-img-with-35" alt="energy">
                                </div>
                            <!-- Affichage description carte 4 -->
                                <h6 class="card-text "><?php  if ($description_carte_4) {?> 
                                    <?php echo ($description_carte_4); ?>  
                                    <?php }?></h6>
                        </div> 
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100">
                        <div class="card-body text-center">
                            <!-- Affichage en-tête carte 5 -->
                                <h5 class="card-title "><?php  if ($titre_carte_5) {?> 
                                    <?php echo $titre_carte_5; ?>  
                                    <?php }?></h5> 
                            <!-- Affichage image carte 5 --> 
                                <div class="card-img-top text-center"> 
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/retourinvestissement.svg" class="card-img-with-35" alt="energy">
                                </div>
                            <!-- Affichage description carte 5 -->
                                <h6 class="card-text "><?php  if ($description_carte_5) {?> 
                                    <?php echo ($description_carte_5); ?>  
                                    <?php }?></h6>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </main>
</div>

<?php get_footer(); ?>