<?php
/**
 * Template Name: Accueil1
 */
?>
<?php get_header(); ?>
<main>
    <?php if( have_posts() ) {
        while( have_posts() ) {
            the_post(); ?>

            <a href="<?php the_permalink()?>">
                <h1><?php the_title(); ?></h1>
                <?php the_post_thumbnail('medium'); ?>
            </a>
            <?php the_excerpt(); ?>
            </div>
        
        <?php  }
         
        } ?>
</main>
<?php get_footer(); ?>
