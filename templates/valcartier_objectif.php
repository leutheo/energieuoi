<?php
/**
 * Template Name: Valcartier_Objectif
 */

?>
<?php get_header(); 

// Caroussel

$caroussel_1 = get_field('caroussel_1');
$picture_caroussel_1 = $caroussel_1['sizes']['my_custom_size_2'];
$caroussel_1_titre = get_field('caroussel_1_titre');
$caroussel_1_description = get_field('caroussel_1_description');

$caroussel_2 = get_field('caroussel_2');
$picture_caroussel_2 = $caroussel_2['sizes']['my_custom_size_2'];
$caroussel_2_titre = get_field('caroussel_2_titre');
$caroussel_2_description = get_field('caroussel_2_description');

$caroussel_3 = get_field('caroussel_3');
$picture_caroussel_3 = $caroussel_3['sizes']['my_custom_size_2'];
$caroussel_3_titre = get_field('caroussel_3_titre');
$caroussel_3_description = get_field('caroussel_3_description');

//Affichage titre 1/
$titre_1 = get_field('titre_1');

$description_1 = get_field('description_1');

$action_1 = get_field('action_1');
$action_2 = get_field('action_2');
$action_3 = get_field('action_3');
$action_4 = get_field('action_4');
$action_5 = get_field('action_5');
$action_6 = get_field('action_6');
$action_7 = get_field('action_7');

//Affichage carte/
$carte_1 = get_field('carte_1');
$carte_2 = get_field('carte_2');
$carte_3 = get_field('carte_3');

//Affichage state/
$titre_state_1 = get_field('titre_state_1');
$sous_titre_state_1 = get_field('sous_titre_state_1');
$state_1 = get_field('state_1');
$state_2 = get_field('state_2');
$state_3 = get_field('state_3');

$titre_state_2 = get_field('titre_state_2');
$state_4 = get_field('state_4');
$state_5 = get_field('state_5');

//Affichage écogeste
$titre_ecogeste_1 = get_field('titre_ecogeste_1');
$sous_titre_ecogeste_1 = get_field('sous_titre_ecogeste_1');
$titre_ecogeste_2 = get_field('titre_ecogeste_2');
$sous_titre_ecogeste_2 = get_field('sous_titre_ecogeste_2');


?>

<div class="page-wrap">
    <!-- Affichage de la bannière image -->

    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="<?php echo $picture_caroussel_1 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_1_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_1_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_2 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_2_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_2_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_3 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_3_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_3_description ?></p>
            </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <main class="container">
        <?php if( have_posts() ) {
            while( have_posts() ) {
                the_post(); ?>
        
                <?php the_content(); ?>
            
            <?php  }
            
            } ?>

        
            <div class="row">
                <div class="row-cols-1 text-center">
                    <!-- Affichage titre 1 -->
                        <h2 class="text-uppercase fs-1 fw-bold text-success m-5 shadow p-3 mb-5 bg-body rounded"><?php  if ($titre_1) {?> 
                        <?php echo $titre_1; ?>  
                        <?php }?></h2> 
                </div>
            </div>
            <div class="row row-cols-1 mt-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <!-- Affichage description 1 -->
                            <span class="card-text "> <?php if ($description_1) { ?>
                                   <?php echo $description_1; ?>
                               <?php } ?>
                            </span>
                            <ul class="list-group">
                                <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_1) { ?>
                                   <?php echo $action_1; ?>
                               <?php } ?></li>
                                <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_2) { ?>
                                   <?php echo $action_2; ?>
                               <?php } ?></li>
                                <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_3) { ?>
                                   <?php echo $action_3; ?>
                               <?php } ?></li>
                                <li class="list-group-item"><i class="fas fa-hand-point-right text-success"></i><?php if ($action_4) { ?>
                                   <?php echo $action_4; ?>
                               <?php } ?>
                                    <ul class="list-group">
                                        <li class="list-group-item"><?php if ($action_5) { ?>
                                            <?php echo $action_5; ?>
                                        <?php } ?></li>
                                        <li class="list-group-item"><?php if ($action_6) { ?>
                                            <?php echo $action_6; ?>
                                        <?php } ?></li>
                                        <li class="list-group-item"></i><?php if ($action_7) { ?>
                                            <?php echo $action_7; ?>
                                        <?php } ?></li>
                                    </ul>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100" >
                        <div class="card-body">
                            <!-- Affichage en-tête carte 1 -->
                            <h4 class="card-title fw-bold"><i class="far fa-times-circle"></i><?php  if ($carte_1) {?> 
                                    <?php echo $carte_1; ?>  
                                    <?php }?></h4>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100">
                        <div class="card-body">
                            <!-- Affichage en-tête carte 2 -->
                            <h4 class="card-title fw-bold"><i class="far fa-times-circle"></i><?php  if ($carte_2) {?> 
                                    <?php echo $carte_2; ?>  
                                    <?php }?></h4> 
                        </div> 
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card h-100">
                        <div class="card-body">
                            <!-- Affichage en-tête carte 3 -->
                            <h4 class="card-title fw-bold"><i class="far fa-times-circle"></i><?php  if ($carte_3) {?> 
                                    <?php echo $carte_3; ?>  
                                    <?php }?></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card h-100" >
                        <div class="card-body">
                            <!-- Affichage description carte 1 -->
                            <h2 class="card-title"><?php  if ($titre_state_1) {?> 
                                    <?php echo ($titre_state_1); ?>  
                                    <?php }?></h2>
                            <!-- Affichage image carte 1 -->
                            <p class="card-title fw-bold"><?php  if ($sous_titre_state_1) {?> 
                                    <?php echo $sous_titre_state_1; ?>  
                                    <?php }?></p>
                            <!-- Affichage en-tête carte 1 -->
                            <p class="card-title "><?php  if ($state_1) {?> 
                                    <?php echo $state_1; ?>  
                                    <?php }?></p>
                            <p class="card-title "><?php  if ($state_2) {?> 
                                    <?php echo $state_2; ?>  
                                    <?php }?></p>
                            <p class="card-title "><?php  if ($state_3) {?> 
                                    <?php echo $state_3; ?>  
                                    <?php }?></p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card h-100">
                        <div class="card-body">
                            <!-- Affichage description carte 2 -->
                            <h2 class="card-title"><?php  if ($titre_state_2) {?> 
                                    <?php echo ($titre_state_2); ?>  
                                    <?php }?></h2>
                            <!-- Affichage en-tête carte 2 -->
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <p class="card-text "><?php  if ($state_4) {?> 
                                    <?php echo $state_4; ?>  
                                    <?php }?></p>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <p class="card-text "><?php  if ($state_5) {?> 
                                    <?php echo $state_5; ?>  
                                    <?php }?></p> 
                        </div> 
                    </div>
                </div>     
            </div>
            <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card mb-3">
                        <div class="row g-0">
                            <div class="col-md-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/doigt.svg" class="top mx-auto d-block card-img-with-35" alt="energy">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <!-- Affichage image carte 1 -->
                                    <h2 class="card-title"><?php  if ($titre_ecogeste_1) {?> 
                                            <?php echo ($titre_ecogeste_1); ?>  
                                            <?php }?></h2>
                                    <p class="card-title fw-bold"><?php  if ($sous_titre_ecogeste_1) {?> 
                                            <?php echo $sous_titre_ecogeste_1; ?>  
                                            <?php }?></p>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card mb-3">
                        <div class="row g-0">
                            <div class="col-md-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/impact.svg" class="top mx-auto d-block card-img-with-35" alt="energy">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h2 class="card-title"><?php  if ($titre_ecogeste_2) {?> 
                                        <?php echo ($titre_ecogeste_2); ?>  
                                        <?php }?></h2>
                                    <p class="card-title fw-bold"><?php  if ($sous_titre_ecogeste_2) {?> 
                                        <?php echo $sous_titre_ecogeste_2; ?>  
                                        <?php }?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>     
                </div>
            </div>
       

    </main>
</div>
<?php get_footer(); ?>