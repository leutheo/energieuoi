<?php
/**
 * Template Name: Valcartier_Mesures
 */
?>
<?php get_header();

// Caroussel

$caroussel_1 = get_field('caroussel_1');
$picture_caroussel_1 = $caroussel_1['sizes']['my_custom_size_2'];
$caroussel_1_titre = get_field('caroussel_1_titre');
$caroussel_1_description = get_field('caroussel_1_description');

$caroussel_2 = get_field('caroussel_2');
$picture_caroussel_2 = $caroussel_2['sizes']['my_custom_size_2'];
$caroussel_2_titre = get_field('caroussel_2_titre');
$caroussel_2_description = get_field('caroussel_2_description');

$caroussel_3 = get_field('caroussel_3');
$picture_caroussel_3 = $caroussel_3['sizes']['my_custom_size_2'];
$caroussel_3_titre = get_field('caroussel_3_titre');
$caroussel_3_description = get_field('caroussel_3_description');


//Affichage titre 1/
$titre_1 = get_field('titre_1');

// Affichage sous-titre A

$sous_titre_1 = get_field('sous_titre_1');

// Affichage bloc 1 

$description = get_field('description');

$sous_titre_2 = get_field('sous_titre_2');

$bouton_1 = get_field('bouton_1');
$bouton_2 = get_field('bouton_2');
$bouton_3 = get_field('bouton_3');
$bouton_4 = get_field('bouton_4');
$bouton_5 = get_field('bouton_5');
$bouton_6 = get_field('bouton_6');
$bouton_7 = get_field('bouton_7');
$bouton_8 = get_field('bouton_8');
$bouton_9 = get_field('bouton_9');
$bouton_10 = get_field('bouton_10');
$bouton_11 = get_field('bouton_11');
$bouton_12 = get_field('bouton_12');
$bouton_13 = get_field('bouton_13');

// Affichage bloc 2 

$description_1 = get_field('description_1');

$sous_titre_3 = get_field('sous_titre_3');

$bouton_14 = get_field('bouton_14');
$bouton_15 = get_field('bouton_15');
$bouton_16 = get_field('bouton_16');
$bouton_17 = get_field('bouton_17');
$bouton_18 = get_field('bouton_18');
$bouton_19 = get_field('bouton_19');
$bouton_20 = get_field('bouton_20');
$bouton_21 = get_field('bouton_21');
$bouton_22 = get_field('bouton_22');
$bouton_23 = get_field('bouton_23');
$bouton_24 = get_field('bouton_24');
$bouton_25 = get_field('bouton_25');
$bouton_26 = get_field('bouton_26');

// Affichage bloc 3 

$sous_titre_4 = get_field('sous_titre_4');

$description_2 = get_field('description_2');

$sous_titre_5 = get_field('sous_titre_5');

$modification_1 = get_field('modification_1');
$modification_2 = get_field('modification_2');
$modification_3 = get_field('modification_3');
$modification_4 = get_field('modification_4');


$bouton_27 = get_field('bouton_27');
$bouton_28 = get_field('bouton_28');
$bouton_29 = get_field('bouton_29');
$bouton_30 = get_field('bouton_30');
$bouton_31 = get_field('bouton_31');
$bouton_32 = get_field('bouton_32');
$bouton_33 = get_field('bouton_33');
$bouton_34 = get_field('bouton_34');
$bouton_35 = get_field('bouton_35');
$bouton_36 = get_field('bouton_36');
$bouton_37 = get_field('bouton_37');
$bouton_38 = get_field('bouton_38');
$bouton_39 = get_field('bouton_39');

// Affichage bloc 4 

$description_3 = get_field('description_3');

$sous_titre_6 = get_field('sous_titre_6');

$modification_5 = get_field('modification_5');
$modification_6 = get_field('modification_6');

$bouton_40 = get_field('bouton_40');
$bouton_41 = get_field('bouton_41');
$bouton_42 = get_field('bouton_42');
$bouton_43 = get_field('bouton_43');
$bouton_44 = get_field('bouton_44');
$bouton_45 = get_field('bouton_45');
$bouton_46 = get_field('bouton_46');
$bouton_47 = get_field('bouton_47');

// Affichage bloc 5

$sous_titre_7 = get_field('sous_titre_7');
$sous_titre_8 = get_field('sous_titre_8');


$modification_7 = get_field('modification_7');
$modification_8 = get_field('modification_8');

$bouton_48 = get_field('bouton_48');
$bouton_49 = get_field('bouton_49');
$bouton_50 = get_field('bouton_50');
$bouton_51 = get_field('bouton_51');
$bouton_52 = get_field('bouton_52');
$bouton_53 = get_field('bouton_53');
$bouton_54 = get_field('bouton_54');
$bouton_55 = get_field('bouton_55');

// Affichage bloc 6

$sous_titre_9 = get_field('sous_titre_9');
$sous_titre_10 = get_field('sous_titre_10');


$modification_9 = get_field('modification_9');
$modification_10 = get_field('modification_10');

$bouton_56 = get_field('bouton_56');
$bouton_57 = get_field('bouton_57');
$bouton_58 = get_field('bouton_58');
$bouton_59 = get_field('bouton_59');
$bouton_60 = get_field('bouton_60');
$bouton_61 = get_field('bouton_61');
$bouton_62 = get_field('bouton_62');
$bouton_63 = get_field('bouton_63');

?>

<div class="page-wrap">
    <!-- Affichage de la bannière image -->

    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="<?php echo $picture_caroussel_1 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_1_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_1_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_2 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_2_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_2_description ?></p>
            </div>
            </div>
            <div class="carousel-item">
            <img src="<?php echo $picture_caroussel_3 ?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block test2">
                <h5 class="text-uppercase text-caroussel"><?php echo $caroussel_3_titre ?></h5>
                <p class="text-uppercase text1-caroussel"><?php echo $caroussel_3_description ?></p>
            </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <main>
        <?php if( have_posts() ) {
            while( have_posts() ) {
                the_post(); ?>
        
                <?php the_content(); ?>
            
            <?php  }
            
            } ?>

        <div class="container">
            <div class="row">
                <div class="row-cols-1 text-center">
                    <!-- Affichage titre 1 -->
                        <h2 class="text-uppercase fs-1 fw-bold text-success m-5 shadow p-3 mb-5 bg-body rounded"><?php  if ($titre_1) {?> 
                        <?php echo $titre_1; ?>  
                        <?php }?></h2> 
                </div>
            </div>
            <div class="row m-5">
                <div class="rowcols-1">
                    <!-- Affichage sous-titre 1 -->
                    <h5 class="card-text text-uppercase text-success "> <?php if ($sous_titre_1) { ?>
                            <?php echo $sous_titre_1; ?>
                        <?php } ?>
                    </h5>
                </div>
                <div class="rowcols-1">
                    <!-- Affichage description -->
                    <p class="card-text desaccord text-success"> <?php if ($description) { ?>
                        <i class="far fa-arrow-alt-circle-right"></i><?php echo $description; ?>
                        <?php } ?>
                    </p>
                </div>
                <div class="accordion" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <div class="rowcols-1">
                                    <!-- Affichage sous-titre 2 -->
                                    <p class="card-text "> <?php if ($sous_titre_2) { ?>
                                        <?php echo $sous_titre_2; ?>
                                        <?php } ?>
                                    </p>
                                </div>
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-5">
                                    <!-- Affichage button -->
                                    <div  class="col"><?php if ($bouton_1) { ?>
                                        <?php echo $bouton_1; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_2) { ?>
                                        <?php echo $bouton_2; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_3) { ?>
                                        <?php echo $bouton_3; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_4) { ?>
                                        <?php echo $bouton_4; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_5) { ?>
                                        <?php echo $bouton_5; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_6) { ?>
                                        <?php echo $bouton_6; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_7) { ?>
                                        <?php echo $bouton_7; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_8) { ?>
                                        <?php echo $bouton_8; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_9) { ?>
                                        <?php echo $bouton_9; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_10) { ?>
                                        <?php echo $bouton_10; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_11) { ?>
                                        <?php echo $bouton_11; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_12) { ?>
                                        <?php echo $bouton_12; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_13) { ?>
                                        <?php echo $bouton_13; ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rowcols-1">
                        <!-- Affichage description 1 -->
                        <p class="card-text desaccord text-success"> <?php if ($description_1) { ?>
                            <i class="far fa-arrow-alt-circle-right"></i><?php echo $description_1; ?>
                            <?php } ?>
                        </p>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <div class="rowcols-1">
                                <!-- Affichage sous-titre 3 -->
                                <p class="card-text "> <?php if ($sous_titre_3) { ?>
                                    <?php echo $sous_titre_3; ?>
                                    <?php } ?>
                                </p>
                            </div>
                        </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-5">
                                    <!-- Affichage button -->
                                    <div  class="col"><?php if ($bouton_14) { ?>
                                        <?php echo $bouton_14; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_15) { ?>
                                        <?php echo $bouton_15; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_16) { ?>
                                        <?php echo $bouton_16; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_17) { ?>
                                        <?php echo $bouton_17; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_18) { ?>
                                        <?php echo $bouton_18; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_19) { ?>
                                        <?php echo $bouton_19; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_20) { ?>
                                        <?php echo $bouton_20; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_21) { ?>
                                        <?php echo $bouton_21; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_22) { ?>
                                        <?php echo $bouton_22; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_23) { ?>
                                        <?php echo $bouton_23; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_24) { ?>
                                        <?php echo $bouton_24; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_25) { ?>
                                        <?php echo $bouton_25; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_26) { ?>
                                        <?php echo $bouton_26; ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-5">
                <div class="rowcols-1">
                        <!-- Affichage sous-titre 4 -->
                        <h5 class="card-text text-uppercase text-success "> <?php if ($sous_titre_4) { ?>
                                <?php echo $sous_titre_4; ?>
                            <?php } ?>
                        </h5>
                </div>
                <div class="rowcols-1">
                    <!-- Affichage description 2 -->
                    <p class="card-text desaccord text-success"> <?php if ($description_2) { ?>
                        <i class="far fa-arrow-alt-circle-right"></i><?php echo $description_2; ?>
                        <?php } ?>
                    </p>
                </div>
                <div class="col-md-12 col-lg-12">
                    <ul class="list-group">
                        <li class="list-group-item"><i class="fas fa-chevron-right text-success"></i><?php if ($modification_1) { ?>
                        <?php echo $modification_1; ?>
                    <?php } ?></li>
                        <li class="list-group-item"><i class="fas fa-chevron-right text-success"></i><?php if ($modification_2) { ?>
                        <?php echo $modification_2; ?>
                    <?php } ?></li>
                        <li class="list-group-item"><i class="fas fa-chevron-right text-success"></i><?php if ($modification_3) { ?>
                        <?php echo $modification_3; ?>
                    <?php } ?></li>
                        <li class="list-group-item"><i class="fas fa-chevron-right text-success"></i><?php if ($modification_4) { ?>
                        <?php echo $modification_4; ?>
                    <?php } ?></li>
                    </ul>
                </div>
                <div class="accordion" id="accordionExample1">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                            <div class="rowcols-1">
                                <!-- Affichage sous-titre 5 -->
                                <p class="card-text "> <?php if ($sous_titre_5) { ?>
                                    <?php echo $sous_titre_5; ?>
                                    <?php } ?>
                                </p>
                            </div>
                        </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample1">
                            <div class="accordion-body">
                                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-5">
                                    <!-- Affichage button -->
                                    <div  class="col"><?php if ($bouton_27) { ?>
                                        <?php echo $bouton_27; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_28) { ?>
                                        <?php echo $bouton_28; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_29) { ?>
                                        <?php echo $bouton_29; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_30) { ?>
                                        <?php echo $bouton_30; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_31) { ?>
                                        <?php echo $bouton_31; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_32) { ?>
                                        <?php echo $bouton_32; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_33) { ?>
                                        <?php echo $bouton_33; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_34) { ?>
                                        <?php echo $bouton_34; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_35) { ?>
                                        <?php echo $bouton_35; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_36) { ?>
                                        <?php echo $bouton_36; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_37) { ?>
                                        <?php echo $bouton_37; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_38) { ?>
                                        <?php echo $bouton_38; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_39) { ?>
                                        <?php echo $bouton_39; ?>
                                        <?php } ?>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                    <div class="rowcols-1">
                        <!-- Affichage description 2 -->
                        <p class="card-text desaccord text-success"> <?php if ($description_3) { ?>
                            <i class="far fa-arrow-alt-circle-right"></i><?php echo $description_3; ?>
                            <?php } ?>
                        </p>
                    </div>
                    <div class="col-md-12 col-lg-12">
                    <ul class="list-group">
                        <li class="list-group-item"><i class="fas fa-chevron-right text-success"></i><?php if ($modification_5) { ?>
                        <?php echo $modification_5; ?>
                    <?php } ?></li>
                        <li class="list-group-item"><i class="fas fa-chevron-right text-success"></i><?php if ($modification_6) { ?>
                        <?php echo $modification_6; ?>
                    <?php } ?></li>
                    </ul>
                </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingFour">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            <div class="rowcols-1">
                                <!-- Affichage sous-titre 5 -->
                                <p class="card-text "> <?php if ($sous_titre_6) { ?>
                                    <?php echo $sous_titre_6; ?>
                                    <?php } ?>
                                </p>
                            </div>
                        </button>
                        </h2>
                        <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample1">
                            <div class="accordion-body">
                                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-5">
                                    <!-- Affichage button -->
                                    <div  class="col"><?php if ($bouton_40) { ?>
                                        <?php echo $bouton_40; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_41) { ?>
                                        <?php echo $bouton_41; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_42) { ?>
                                        <?php echo $bouton_42; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_43) { ?>
                                        <?php echo $bouton_43; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_44) { ?>
                                        <?php echo $bouton_44; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_45) { ?>
                                        <?php echo $bouton_45; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_46) { ?>
                                        <?php echo $bouton_46; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_47) { ?>
                                        <?php echo $bouton_47; ?>
                                        <?php } ?>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-5">
                <div class="rowcols-1">
                        <!-- Affichage sous-titre 4 -->
                        <h5 class="card-text text-uppercase text-success "> <?php if ($sous_titre_7) { ?>
                                <?php echo $sous_titre_7; ?>
                            <?php } ?>
                        </h5>
                </div>
                <div class="accordion" id="accordionExample2">
                    <div class="col-md-12 col-lg-12">
                        <ul class="list-group">
                            <li class="list-group-item"><i class="fas fa-chevron-right text-success"></i><?php if ($modification_7) { ?>
                            <?php echo $modification_7; ?>
                        <?php } ?></li>
                            <li class="list-group-item"><i class="fas fa-chevron-right text-success"></i><?php if ($modification_8) { ?>
                            <?php echo $modification_8; ?>
                        <?php } ?></li>
                        </ul>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingFive">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            <div class="rowcols-1">
                                <!-- Affichage sous-titre 5 -->
                                <p class="card-text "> <?php if ($sous_titre_8) { ?>
                                    <?php echo $sous_titre_8; ?>
                                    <?php } ?>
                                </p>
                            </div>
                        </button>
                        </h2>
                        <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample2">
                            <div class="accordion-body">
                                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-5">
                                    <!-- Affichage button -->
                                    <div  class="col"><?php if ($bouton_48) { ?>
                                        <?php echo $bouton_48; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_49) { ?>
                                        <?php echo $bouton_49; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_50) { ?>
                                        <?php echo $bouton_50; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_51) { ?>
                                        <?php echo $bouton_51; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_52) { ?>
                                        <?php echo $bouton_52; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_53) { ?>
                                        <?php echo $bouton_53; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_54) { ?>
                                        <?php echo $bouton_54; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_55) { ?>
                                        <?php echo $bouton_55; ?>
                                        <?php } ?>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-5">
                <div class="rowcols-1">
                        <!-- Affichage sous-titre 4 -->
                        <h5 class="card-text text-uppercase text-success "> <?php if ($sous_titre_9) { ?>
                                <?php echo $sous_titre_9; ?>
                            <?php } ?>
                        </h5>
                </div>
                <div class="accordion" id="accordionExample3">
                    <div class="col-md-12 col-lg-12">
                        <ul class="list-group">
                            <li class="list-group-item"><i class="fas fa-chevron-right text-success"></i><?php if ($modification_9) { ?>
                            <?php echo $modification_9; ?>
                        <?php } ?></li>
                            <li class="list-group-item"><i class="fas fa-chevron-right text-success"></i><?php if ($modification_10) { ?>
                            <?php echo $modification_10; ?>
                        <?php } ?></li>
                        </ul>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingSix">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            <div class="rowcols-1">
                                <!-- Affichage sous-titre 5 -->
                                <p class="card-text "> <?php if ($sous_titre_10) { ?>
                                    <?php echo $sous_titre_10; ?>
                                    <?php } ?>
                                </p>
                            </div>
                        </button>
                        </h2>
                        <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix" data-bs-parent="#accordionExample3">
                            <div class="accordion-body">
                                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-5">
                                    <!-- Affichage button -->
                                    <div  class="col"><?php if ($bouton_56) { ?>
                                        <?php echo $bouton_56; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_57) { ?>
                                        <?php echo $bouton_57; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_58) { ?>
                                        <?php echo $bouton_58; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_59) { ?>
                                        <?php echo $bouton_59; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_60) { ?>
                                        <?php echo $bouton_60; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_61) { ?>
                                        <?php echo $bouton_61; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_62) { ?>
                                        <?php echo $bouton_62; ?>
                                        <?php } ?>
                                    </div>
                                    <div  class="col"><?php if ($bouton_63) { ?>
                                        <?php echo $bouton_63; ?>
                                        <?php } ?>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<?php get_footer(); ?>