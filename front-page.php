<?php get_header(); ?>
<div class="page-wrap">
    <div class="row">
        <div class="row-cols-1 text-center">
            <h1 class="text-uppercase fs-1 fw-bold text-success m-5">
                <?php bloginfo('name'); ?>
            </h1>
        </div>
    </div>
    
    <main id="accueil" class="container">
        <!-- Affcihe le titre -->
        
        <?php if( have_posts() ) {
            while( have_posts() ) {
                the_post(); ?>
                <article>
                        <!-- Lien vers article -->
                        <a href="<?php the_permalink()?>">
                            <?php the_post_thumbnail('medium'); ?>
                            <h2><?php the_title(); ?></h2>
                        </a>
                        <?php the_excerpt(); ?>
                        <a href="<?php the_permalink()?>">
                            Lire la suite
                        </a>
                    </article>
            <?php  }
            
            } ?>
    </main>
</div>
    
<?php get_footer(); ?>