<?php get_header(); ?>
    <div class="page-wrap">
        <main class="container">
            <?php if( have_posts() ) {
                while( have_posts() ){
                the_post(); ?>
                    <div class="row">
                        <div class="row-cols-1 text-center">
                            <h1 class="text-uppercase fs-1 fw-bold text-success m-5">
                                <?php the_title(); ?>
                            </h1>
                        </div>
                    </div>
                    <?php the_post_thumbnail('large'); ?>
                    <div class="post__content m-5">
                        <?php the_content(); ?>
                    </div>

                <?php comments_template(); // Par ici les commentaires ?>

                <?php get_template_part( 'template_parts/newsletter' ); ?>

                <?php } 
            } ?>
        </main>
    </div>
    
<?php get_footer(); ?>