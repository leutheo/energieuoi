<footer>
    <div id="menu-footer">
        <div class="logo-footer">
            <a href="<?php echo home_url( '/' ); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/canada-logo_vert.svg" alt="Logo">
            </a>
        </div>
        <!-- Affichage du menu footer -->
        <?php wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?>
    </div>
</footer> 
<div id="footer2"  >
    <div class="row row-cols-1 row-cols-md-3  container footer3">
        <div class="col-12 col-md-6 col-lg-6">
            <div >
                <div class="card-body footer2"> 
                    <a href="https://www.canada.ca/fr/ministere-defense-nationale.html" target="_blank">
                        <div class="footer-link">
                            <p>Ministère de la défense nationale</p>
                            <i class="bi bi-box-arrow-up-right"></i>
                        </div>   
                    </a> 
                </div>
            </div>
        </div>
        
        <div class="col-12 col-md-6 col-lg-6">
            <div >
                <div class="card-body footer2 text-end">
                    <a href="#">
                        <p>© Unités des Opérations Immobilières, Québec</p>
                    </a>
                </div>
            </div>
        </div>
    </div> 
</div>


<!-- Vous pourriez ajouter votre script Google Analytics ici -->

<!-- Bootsrap Javascript -->
<?php wp_footer(); ?>

</body>
</html>