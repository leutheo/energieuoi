<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'energieuoi' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'wF,kj=o`elipe<421YOz#(B|Kcl8ydEca`-ZH}fVB*Jqrc*kk,G4%EYqa[?ueS7M' );
define( 'SECURE_AUTH_KEY',  '{6+76pnRi-[VtFk8-EXiC.Giv}sn9EO,E.NH78VE>gih3MI,6!;7$t^;djKt|g2@' );
define( 'LOGGED_IN_KEY',    'z*BV#uM;_#F?. h<YCb<1hi_S&=6^(G]tU[lZxXb7}+hx]mYI+nC)efV<Vgx|L4T' );
define( 'NONCE_KEY',        'KjA? |O_&=[I9j6}.xd1F`lOB )?KwQ9WV}zv.*hP0#Wn*BOy9kE7mlo/A)!!*@-' );
define( 'AUTH_SALT',        '~sFUc52*q}a8|/B*Yhr(wPLIP-fZ+>K%}Kn55:2WfhhfW&a*22[-nkr?-7olUBK`' );
define( 'SECURE_AUTH_SALT', '4Hbiw//ah2*0~)?rG<%Y~_b3O&G FmH`(~_CDk`_S/!!0ii?Ig{|H]5X/2XD;,UC' );
define( 'LOGGED_IN_SALT',   'u.GpMePXg]K_IWYFS9wSOzr|a&&x&o%b$i534S5TPWNgDJb: 4mjihTT%pK!5j_m' );
define( 'NONCE_SALT',       'jjA;<A,XEea5z86|#lL QBh]F?M4.K<MNmFmx8My- NP$*M3&T%Q-{{3Y%SVl,=;' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'euoi_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
